package cmd

import (
	"fmt"
	"log"
	"os"
	"slices"
	"strconv"
	"time"

	jira "github.com/andygrunwald/go-jira"
	"github.com/mitchellh/mapstructure"
	"github.com/spf13/cobra"

	// This is useful with spew.Dump(struct), ie) spew.Dump(issue)
	"github.com/davecgh/go-spew/spew"
)

var (
	showCustomFieldIDs bool
	userSelectedFields []string
	noEscapedText      bool
	showEmptyFields    bool
	debug              bool
)

func outputField(name string, value interface{}) {
	var valueStr interface{}

	if len(userSelectedFields) != 0 && !contains(userSelectedFields, name) {
		return
	}

	switch valueType := value.(type) {
	case string:
		valueStr = valueType
	case int:
		valueStr = strconv.Itoa(valueType)
	case jira.Time:
		valueStr = time.Time(valueType).Format(timeFormat)
	case *jira.User:
		if valueType != nil {
			valueStr = valueType.DisplayName + " <" + valueType.Name + ">"
		} else {
			valueStr = ""
		}
	}

	fmt.Printf("FIELD[%s]: %s\n", name, valueStr)
}

func unknownFieldWarning(issue *jira.Issue, field jira.Field, err error) {
	fmt.Printf("\n-------- 8< cut here 8< --------\n")
	fmt.Println("Warning:", err)
	fmt.Println("Warning: UNKNOWN FIELD TYPE.  Please cut-and-paste this output and report this to https://gitlab.com/prarit/rhjira/-/issues")
	fmt.Printf("Issue ID: %s [%s]\n", issue.ID, time.Now().String())
	fmt.Printf(" The data is field.ID[%s] and schemaType[%s]\n",
		field.ID, field.Schema.Type)
	fmt.Printf(" Data dump for field.Name[%s]:\n", field.Name)
	spew.Dump(issue.Fields.Unknowns[field.ID])
	fmt.Println("-------- 8< end here 8< --------")
}

func dumpChildOption(entry interface{}) string {
	type childOption struct {
		Self     string
		Value    string
		Id       string
		Disabled bool
		Child    struct {
			Value    string
			Id       string
			Disabled bool
			Self     string
		}
	}

	var u childOption
	err := mapstructure.Decode(entry, &u)
	if err != nil {
		log.Fatal(err)
	}

	return u.Value + "-" + u.Child.Value
}
func dumpOption(entry interface{}) string {
	type option struct {
		Value    string
		Id       string
		Disabled bool // this means disabled in search
		Self     string
	}

	var u option
	err := mapstructure.Decode(entry, &u)
	if err != nil {
		log.Fatal(err)
	}
	return u.Value
}

func dumpUser(entry interface{}) string {
	var u jira.User
	err := mapstructure.Decode(entry, &u)
	if err != nil {
		log.Fatal(err)
	}
	if u.EmailAddress != "" {
		return fmt.Sprintf("%s <%s>", u.Name, u.EmailAddress)
	}
	return u.Name
}

func dumpVotes(entry interface{}) string {
	type votes struct {
		HasVoted bool
		Self     string
		Votes    float64
	}

	var u votes
	err := mapstructure.Decode(entry, &u)
	if err != nil {
		log.Fatal(err)
	}
	return fmt.Sprintf("%f", u.Votes)
}

func dumpSecurityLevel(entry interface{}) string {
	type securitylevel struct {
		Description string
		Id          string
		Name        string
		Self        string
	}

	var u securitylevel
	err := mapstructure.Decode(entry, &u)
	if err != nil {
		log.Fatal(err)
	}
	return u.Description
}

func dumpVersion(entry interface{}) string {
	type version struct {
		Archived    bool
		Description string
		ID          string
		Name        string
		released    bool   //nolint:unused
		self        string //nolint:unused
	}

	var u version
	err := mapstructure.Decode(entry, &u)
	if err != nil {
		log.Fatal(err)
	}
	return u.Name
}

func evaluateField(issue jira.Issue, fieldID string, entry interface{}, schemaType string) (string, error) {
	if entry == nil {
		return "", nil
	}

	if debug {
		fmt.Println("evaluateField called for fieldID: " + fieldID)
		fmt.Println("schemaType: " + schemaType)
		spew.Dump(entry)
	}

	switch schemaType {
	case "any":
		// PRARIT: customfield_12316840/"Bugzilla Bug"
		if fieldID == "customfield_12316840" {
			type customfield_12316840 struct {
				SiteID float64
				BugID  float64
			}
			var u customfield_12316840
			err := mapstructure.Decode(entry, &u)
			if err != nil {
				log.Fatal(err)
			}
			return fmt.Sprintf("%d", int(u.BugID)), nil
		}
		// 'any' appears to be a string by default
		return fmt.Sprintf("%s", entry), nil
	case "array":
		// this is part of an array of values
		// PRARIT customfield_12311840/"Need Info From"
		if fieldID == "customfield_12311840" {
			return dumpUser(entry), nil
		}
		// PRARIT customfield_12323140/"Target Version"
		if fieldID == "customfield_12323140" {
			return dumpVersion(entry), nil
		}
		switch entry.(type) {
		case string:
			return fmt.Sprintf("%s", entry), nil
		default:
			return dumpOption(entry), nil
		}
	case "date":
		// 'date' is really just a string conforming to YYYY-MM-DD
		return fmt.Sprintf("%s", entry), nil
	case "datetime":
		// 'date' is really just a string
		// ex) "2019-12-25T02:51:00.000+0000"
		return fmt.Sprintf("%s", entry), nil
	case "number":
		switch entry.(type) {
		case float64:
			return fmt.Sprintf("%f", entry), nil
		case string:
			return fmt.Sprintf("%s", entry), nil
		}
	case "sd-approvals":
		// PRARIT, cannot find any tickets with this field set.  For now just return
		// an empty string
		return "", nil
	case "sd-customerrequesttype":
		// PRARIT, cannot find any tickets with this field set.  For now just return
		// an empty string
		return "", nil
	case "sd-servicelevelagreement":
		// PRARIT, cannot find any tickets with this field set.  For now just return
		// an empty string
		return "", nil
	case "securitylevel":
		return dumpSecurityLevel(entry), nil
	case "string":
		return fmt.Sprintf("%s", entry), nil
	case "option":
		return dumpOption(entry), nil
	case "option-with-child":
		return dumpChildOption(entry), nil
	case "user":
		return dumpUser(entry), nil
	case "version":
		return dumpVersion(entry), nil
	case "votes":
		return dumpVotes(entry), nil

	}
	return "", fmt.Errorf("schemaType=%s not handled in evaluateField()\n", schemaType)
}

// This is a list of all known Internal fields
var knownInternalFields = []string{
	"aggregateprogress",
	"aggregatetimeestimate",
	"aggregatetimeoriginalestimate",
	"aggregatetimespent",
	"archivedby",   // Not implemented in go-jira (see customOverrides)
	"archiveddate", // Not implemented in go-jira (see customOverrides)
	"assignee",
	"attachment", // Not implemented in go-jira (see customOverrides)
	"comment",
	"components",
	"created",
	"creator",
	"description",
	"duedate",
	"environment",
	"epic",
	"fixVersions",
	"issuekey",
	"issuelinks",
	"issuetype",
	"labels",
	"lastViewed", // Not implemented in go-jira (see customOverrides)
	"parent",
	"priority",
	"project",
	"progress",
	"remotelinks",
	"reporter",
	"resolution",
	"resolutiondate",
	"security", // Not implemented in go-jira (see customOverrides)
	"sprint",
	"status",
	"statusCategory",
	"subtasks", // PRARIT: may need to be adjusted when we actually use them
	"summary",
	"thumbnail", // Not implemented in go-jira (see customOverrides)
	"timeestimate",
	"timeoriginalestimate",
	"timetracking", // PRARIT: may need to be adjusted when we actually use them
	"timespent",
	"updated",
	"versions", //AffectsVersions, Affects Versions/s
	"votes",    // Not implemented in go-jira (see customOverrides)
	"watches",
	"worklog",   // PRARIT: Not implemented.  See GetWorklog().
	"workratio", // Not implemented
	"security",  // Not implemented in go-jira (see customOverrides)
}

// This is a list of jira built-in fields that are not implemented in go-jira.  These
// can be evaluated in the 'custom field' loop below as exceptions
var customOverrides = []string{
	"archivedby",
	"archiveddate",
	"attachment",
	"lastViewed",
	"thumbnail",
	"votes",
	"workratio",
	"security",
}

var dumpCmd = &cobra.Command{
	Use:     "dump",
	Short:   "Dump jira issue variables.",
	Aliases: []string{"raw"},
	Long:    ``,
	Args:    cobra.RangeArgs(1, 1),
	Run: func(cmd *cobra.Command, args []string) {

		if contains(args, "help") {
			_ = cmd.Help()
			os.Exit(0)
		}

		issueID := args[0]

		issue, _, err := JiraClient.Sprint.GetIssue(issueID, nil)
		if err != nil {
			log.Fatal(err)
		}

		// this retrieves the list of fields for issues
		fieldList, _, err := JiraClient.Field.GetList()
		if err != nil {
			log.Fatal(err)
		}

		if debug {
			fmt.Println("issue +++++++++++++++++++++++++++++++++++++++++++++++++++")
			// the output of the issue.Fields.Unknowns contains the values
			// of all custom fields.  So there is no further lookup
			// necessary
			spew.Dump(issue)

			fmt.Println("fieldList ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
			spew.Dump(fieldList)

			fmt.Println("field.ID/Field.Name to schema type +++++++++++++++++++++++++++++++++++++++++++++++++++")
			fmt.Println("Search-Parameter Name Type")
			for _, field := range fieldList {
				fmt.Printf("%s \"%s\" %s\n", field.ID, field.Name, field.Schema.Type)
			}

			fmt.Println("Built-in field.ID/Field.Name to schema type +++++++++++++++++++++++++++++++++++++++++++")
			fmt.Println("Search-Parameter Name Type")
			for _, field := range fieldList {
				if field.Custom {
					continue
				}
				fmt.Printf("%s \"%s\" %s\n", field.ID, field.Name, field.Schema.Type)
			}

			fmt.Println("missing internal (non-custom) fields ++++++++++++++++++++++++++++++++++++++++++++++++++")
			for _, field := range fieldList {
				if field.Custom == true {
					continue
				}
				// add this to an array, and compare the array with the static one
				if !slices.Contains(knownInternalFields, field.ID) {
					fmt.Printf("\n-------- 8< cut here 8< --------\n")
					fmt.Printf("Warning: %s not in knownInternalFields.  This field likely has it's own accessor in the issue struct.\n", field.ID)
					spew.Dump(field)
					fmt.Printf("\n-------- 8< cut here 8< --------\n")
				}
			}
			fmt.Println("unknowns ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
		}

		//
		// Internal Fields (ie, non-custom fields)
		//
		outputField("aggregateprogress", GetAggregateProgress(issue))
		outputField("aggregatetimeestimate", GetAggregateTimeEstimate(issue))
		outputField("aggregatetimeoriginalestimate", GetAggregateTimeOriginalEstimate(issue))
		outputField("aggregatetimespent", GetAggregateTimeSpent(issue))
		outputField("assignee", GetAssignee(issue))
		if noEscapedText {
			outputField("comment", GetComments(issue))
		} else {
			outputField("comment", strconv.Quote(GetComments(issue)))
		}
		outputField("components", GetComponents(issue))
		outputField("created", GetCreated(issue))
		outputField("creator", GetCreator(issue))
		if noEscapedText {
			outputField("description", GetDescription(issue))
		} else {
			outputField("description", strconv.Quote(GetDescription(issue)))
		}
		outputField("duedate", GetDuedate(issue))
		outputField("environment", GetEnvironment(issue))
		outputField("epic", GetEpic(issue))
		outputField("fixVersions", GetFixVersions(issue))
		outputField("labels", GetLabels(issue))
		outputField("issuekey", issue.Key)
		outputField("issuelinks", GetIssueLinks(issue))
		outputField("issuetype", GetIssueType(issue))
		outputField("parent", GetParent(issue))
		outputField("priority", GetPriority(issue))
		outputField("progress", GetProgress(issue))
		outputField("project", GetProject(issue))
		outputField("remotelinks", GetRemoteLinks(issue))
		outputField("reporter", GetReporter(issue))
		outputField("resolution", GetResolution(issue))
		outputField("resolutiondate", GetResolutionDate(issue))
		outputField("sprint", GetSprint(issue))
		outputField("status", GetStatus(issue))
		outputField("subtasks", GetSubtasks(issue))
		outputField("summary", GetSummary(issue))
		outputField("timeestimate", GetTimeEstimate(issue))
		outputField("timeoriginalestimate", GetTimeOriginalEstimate(issue))
		outputField("timespent", GetTimeSpent(issue))
		outputField("timetracking", GetTimeTracking(issue))
		outputField("watches", GetWatches(issue))
		outputField("updated", GetUpdated(issue))
		outputField("versions", GetAffectsVersions(issue))

		// custom fields
		for index, field := range fieldList {

			if !field.Custom && !contains(customOverrides, field.ID) {
				continue
			}

			fieldName := field.Name
			if showCustomFieldIDs {
				fieldName = field.Name + " | " + field.ID
			}

			// if field is nil, output empty
			if issue.Fields.Unknowns[field.ID] == nil {
				if showEmptyFields {
					outputField(fieldName, "")
				}
				continue
			}

			var valueStr string
			switch issue.Fields.Unknowns[field.ID].(type) {
			case []interface{}:
				entries := issue.Fields.Unknowns[field.ID].([]interface{})
				for _, entry := range entries {
					valueStr, err = evaluateField(*issue, field.ID, entry, fieldList[index].Schema.Type)
					if err != nil {
						unknownFieldWarning(issue, field, err)
						outputField(fieldName, "")
						continue
					}
				}
			default:
				valueStr, err = evaluateField(*issue, field.ID, issue.Fields.Unknowns[field.ID], fieldList[index].Schema.Type)
				if err != nil {
					unknownFieldWarning(issue, field, err)
					outputField(fieldName, "")
					continue
				}
			}
			outputField(fieldName, valueStr)
		}
	}, // end of func(cmd *cobra.Command, args []string)
}

func init() {
	dumpCmd.Flags().BoolVarP(&showCustomFieldIDs, "showcustomfields", "", false, "show the customfield IDs and the customfield names")
	dumpCmd.Flags().BoolVarP(&showEmptyFields, "showemptyfields", "", false, "show all fields including those that are not defined for this issue")
	dumpCmd.Flags().StringSliceVarP(&userSelectedFields, "fields", "", []string{}, "specify a comma-separated list of fields for output")
	dumpCmd.Flags().BoolVarP(&noEscapedText, "noescapedtext", "", false, "show dump text (ie, no escaped characters)")
	dumpCmd.Flags().BoolVarP(&debug, "debug", "d", false, "verbose debug output")
	_ = dumpCmd.Flags().MarkHidden("debug")

	RootCmd.AddCommand(dumpCmd)
}
