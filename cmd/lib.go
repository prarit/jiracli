package cmd

import (
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	jira "github.com/andygrunwald/go-jira"
	// This is useful with spew.Dump(struct), ie) spew.Dump(issue)
	//"github.com/davecgh/go-spew/spew"
)

var (
	timeFormat   string = "2006-01-02 15:04:05"
	timeFormatTZ string = "2006-01-02T15:04:05.000+0000"
	baseURL      string = "https://issues.redhat.com/browse/"
)

func contains(names []string, name string) bool {
	for _, a := range names {
		if a == name {
			return true
		}
	}
	return false
}

func link(text string, URL string) string {
	return fmt.Sprintf("\033]8;;%s\a%s\033]8;;\a\033[0;37m", URL, text)
}

func GetCustomFields(issueID string, existingFields map[string]string) (allFields map[string]string, issueFields jira.CustomFields) {

	// this retrieves the list of fields for issues
	dumpfields, _, err := JiraClient.Field.GetList()
	if err != nil {
		log.Fatal(err)
	}

	// allFields maps customfieldXXXX to a name, ie) Devel Target Milestone
	if len(allFields) == 0 {
		allFields = make(map[string]string)
		for _, field := range dumpfields {
			allFields[field.ID] = field.Name
		}
	} else {
		allFields = existingFields
	}

	// issueFields maps the customfieldXXXX to an actual value for the issue
	issueFields, _, err = JiraClient.Issue.GetCustomFields(issueID)
	if err != nil {
		log.Fatal(err)
	}

	return allFields, issueFields
}

// ==========
func GetAggregateProgress(issue *jira.Issue) string {
	return fmt.Sprintf("%d%%", issue.Fields.AggregateProgress.Percent)
}

func GetAggregateTimeEstimate(issue *jira.Issue) string {
	return strconv.Itoa(issue.Fields.AggregateTimeEstimate)
}

func GetAggregateTimeOriginalEstimate(issue *jira.Issue) string {
	return strconv.Itoa(issue.Fields.AggregateTimeOriginalEstimate)
}

func GetAggregateTimeSpent(issue *jira.Issue) string {
	return strconv.Itoa(issue.Fields.AggregateTimeSpent)
}

func GetAssignee(issue *jira.Issue) *jira.User {
	return issue.Fields.Assignee
}

// The displaying of the comments in the CLI requires a choice to be made on
// how the comments should be displayed.  I could display them one-by-one, in
// some sort of bulleted fashion, but I've opted for this "paragraph" type
// view. Some people aren't going to like it.  If you're about to complain
// about it: Don't.
func GetComments(issue *jira.Issue) string {
	Body := ""
	for _, c := range issue.Fields.Comments.Comments {
		author := c.Author.DisplayName + " <" + c.Author.Name + ">"
		utime, err := time.Parse(timeFormatTZ, c.Updated)
		if err != nil {
			fmt.Println(err)
			break
		}

		ctime, err := time.Parse(timeFormatTZ, c.Created)
		if err != nil {
			fmt.Println(err)
			break
		}
		commentNote := "Created by"
		if utime.After(ctime) {
			commentNote = "Updated by"
		}

		commentNote += " " + author + " at " + utime.Format(timeFormat)
		Body = Body + commentNote + " :\n" + c.Body + "\n\n"
	}
	return Body
}

func GetComponents(issue *jira.Issue) string {
	var values []string
	for _, c := range issue.Fields.Components {
		values = append(values, c.Name)
	}
	return strings.Join(values, ",")
}

func GetCreated(issue *jira.Issue) jira.Time {
	return issue.Fields.Created
}

func GetCreator(issue *jira.Issue) *jira.User {
	return issue.Fields.Creator
}

func GetDescription(issue *jira.Issue) string {
	return issue.Fields.Description
}

func GetDuedate(issue *jira.Issue) string {
	dateStr := fmt.Sprintf("%s", time.Time(issue.Fields.Duedate).Format("2006-01-02"))
	// uninitialized date is 0001-01-01
	if dateStr == "0001-01-01" {
		return ""
	}
	return dateStr
}

func GetEnvironment(issue *jira.Issue) string {
	return issue.Fields.Environment
}

func GetEpic(issue *jira.Issue) string {
	if issue.Fields.Epic != nil {
		return issue.Fields.Epic.Key
	}
	return ""
}

func GetFixVersions(issue *jira.Issue) string {
	var values []string
	for _, f := range issue.Fields.FixVersions {
		values = append(values, f.Name)
	}
	return strings.Join(values, ",")
}

func GetIssueLinks(issue *jira.Issue) string {
	body := ""
	for count, link := range issue.Fields.IssueLinks {
		if link.InwardIssue != nil {
			body += link.Type.Inward + " " + baseURL + link.InwardIssue.Key
		}
		if link.OutwardIssue != nil {
			body += link.Type.Outward + " " + baseURL + link.OutwardIssue.Key
		}
		if count != (len(issue.Fields.IssueLinks) - 1) {
			body += ", "
		}
	}
	return body
}

// This is a made up field that represents the issue type.  It is useful to be
// able to search for Bug, Story, Epic, etc.
func GetIssueType(issue *jira.Issue) string {
	return issue.Fields.Type.Name
}

func GetLabels(issue *jira.Issue) string {
	return strings.Join(issue.Fields.Labels, ",")
}

func GetParent(issue *jira.Issue) string {
	if issue.Fields.Parent != nil {
		return issue.Fields.Parent.Key
	}
	return ""
}

func GetPriority(issue *jira.Issue) string {
	return issue.Fields.Priority.Name
}

func GetProject(issue *jira.Issue) string {
	return issue.Fields.Project.Name
}

func GetProgress(issue *jira.Issue) string {
	return fmt.Sprintf("%d%%", issue.Fields.Progress.Percent)
}

func GetRemoteLinks(issue *jira.Issue) string {
	body := ""
	// Remote Links are a whole different query
	remoteLinks, _, err := JiraClient.Issue.GetRemoteLinks(issue.ID)
	if err != nil {
		log.Fatal(err)
	}
	for count, r := range *remoteLinks {
		relationship := r.Relationship
		if relationship == "" {
			relationship = "links to"
		}
		body += relationship + " " + r.Object.URL
		if (count + 1) < len(*remoteLinks) {
			body += ", "
		}
	}
	return body
}

func GetReporter(issue *jira.Issue) *jira.User {
	return issue.Fields.Reporter
}

func GetResolution(issue *jira.Issue) string {
	// The resolution struct only exists if the ticket has been closed
	if issue.Fields.Resolution == nil {
		return ""
	}
	return issue.Fields.Resolution.Name
}

func GetResolutionDate(issue *jira.Issue) jira.Time {
	return issue.Fields.Resolutiondate
}

func GetSprint(issue *jira.Issue) string {
	if issue.Fields.Sprint != nil {
		return issue.Fields.Sprint.Name
	}
	return ""
}

func GetStatus(issue *jira.Issue) string {
	return issue.Fields.Status.Name
}

func GetSubtasks(issue *jira.Issue) string {
	body := ""
	for count, task := range issue.Fields.Subtasks {
		body += task.Key
		if count != (len(issue.Fields.Subtasks) - 1) {
			body += ", "
		}
	}
	return body
}

func GetSummary(issue *jira.Issue) string {
	return issue.Fields.Summary
}

func GetTimeEstimate(issue *jira.Issue) string {
	return strconv.Itoa(issue.Fields.TimeEstimate)
}

func GetTimeOriginalEstimate(issue *jira.Issue) string {
	return strconv.Itoa(issue.Fields.TimeOriginalEstimate)
}

func GetTimeTracking(issue *jira.Issue) string {
	body := ""
	body += fmt.Sprintf("OriginalEstimate: %s ", issue.Fields.TimeTracking.OriginalEstimate)
	body += fmt.Sprintf("RemainingEstimate: %s ", issue.Fields.TimeTracking.RemainingEstimate)
	body += fmt.Sprintf("TimeSpent: %s ", issue.Fields.TimeTracking.TimeSpent)
	body += fmt.Sprintf("OriginalEstimateSeconds: %d ", issue.Fields.TimeTracking.OriginalEstimateSeconds)
	body += fmt.Sprintf("RemainingEstimateSeconds: %d ", issue.Fields.TimeTracking.RemainingEstimateSeconds)
	body += fmt.Sprintf("TimeSpentSeconds: %d ", issue.Fields.TimeTracking.TimeSpentSeconds)
	return body
}

func GetTimeSpent(issue *jira.Issue) string {
	return strconv.Itoa(issue.Fields.TimeSpent)
}

func GetUpdated(issue *jira.Issue) jira.Time {
	return issue.Fields.Updated
}

func GetAffectsVersions(issue *jira.Issue) string {
	var values []string
	for _, f := range issue.Fields.AffectsVersions {
		values = append(values, f.Name)
	}
	return strings.Join(values, ",")
}

func GetWatches(issue *jira.Issue) string {
	var values []string
	if issue.Fields.Watches.IsWatching {
		for _, w := range issue.Fields.Watches.Watchers {
			values = append(values, w.Name)
		}
		return strings.Join(values, ",")
	}
	return ""
}

func GetWorklog(issue *jira.Issue) string {
	// Worklog only exists if it was used
	if issue.Fields.Worklog == nil {
		return ""
	}
	return "Please open a ticket at https://gitlab.com/prarit/rhjira for implementation."
}
