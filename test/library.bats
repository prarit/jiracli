#!/usr/bin/env bats

check_status() {
	if [ "$status" -eq 0 ]; then
		return 0
	fi

	echo "ERROR"
	echo "output=$output"
	exit 1
}

data=$(rhjira dump --showcustomfields --showemptyfields RHEL-56971)
grepdata() {
	echo "$data" | grep "$1"
}
