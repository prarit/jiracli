load ./library.bats

@test "compile-rhjira" {
	cd ..
	run make
	check_status
	cp rhjira test/
}

@test "aggregateprogress" {
	run grepdata "FIELD\[aggregateprogress\]: 0%"
	check_status
}

@test "aggregatetimeestimate" {
	run grepdata "FIELD\[aggregatetimeestimate\]: 0"
	check_status
}

@test "aggregatetimeoriginalestimate" {
	run grepdata "FIELD\[aggregatetimeoriginalestimate\]: 0"
	check_status
}

@test "aggregatetimespent" {
	run grepdata "FIELD\[aggregatetimespent\]: 0"
	check_status
}

@test "assignee" {
	run grepdata "FIELD\[assignee\]: Prarit Bhargava <prarit@redhat.com>"
	check_status
}

@test "comment" {
	run grepdata "FIELD\[comment\]: \"Created by Niels De Graef <ndegraef@redhat.com> at 2024-09-03 12:22:34 :\\\nplease ignore, funky watson stuff was going on here\\\n\\\n\""
	check_status
}

@test "components" {
	run grepdata "FIELD\[components\]: kernel / Platform Enablement / x86_64"
	check_status
}

@test "created" {
	run grepdata "FIELD\[created\]: 2024-09-03 11:34:05"
	check_status
}

@test "creator" {
	run grepdata "FIELD\[creator\]: Watson Automation <watson-automation>"
	check_status
}

@test "description" {
	run grepdata "FIELD\[description\]: \"This is a clone of issue RHEL-25415 to use for version rhel-9.6\\\n--\\\nOriginal description:\\\nUpdate arch/x86 to upstream 6.7.\""
	check_status
}

@test "duedate" {
	run grepdata "FIELD\[duedate\]:"
	check_status
}

@test "environment" {
	run grepdata "FIELD\[environment\]:"
	check_status
}

@test "epic" {
	run grepdata "FIELD\[epic\]:"
	check_status
}

@test "fixVersions" {
	run grepdata "FIELD\[fixVersions\]: rhel-9.6"
	check_status
}

@test "labels" {
	run grepdata "FIELD\[labels\]:"
	check_status
}

@test "issuekey" {
	run grepdata "FIELD\[issuekey\]: RHEL-56971"
	check_status
}

@test "issuelinks" {
	run grepdata "FIELD\[issuelinks\]: clones https://issues.redhat.com/browse/RHEL-25415"
	check_status
}

@test "issuetype" {
	run grepdata "FIELD\[issuetype\]: Story"
	check_status
}

@test "parent" {
	run grepdata "FIELD\[parent\]:"
	check_status
}

@test "priority" {
	run grepdata "FIELD\[priority\]: Undefined"
	check_status
}

@test "progress" {
	run grepdata "FIELD\[progress\]: 0%"
	check_status
}

@test "project" {
	run grepdata "FIELD\[project\]: RHEL"
	check_status
}

@test "remotelinks" {
	run grepdata "FIELD\[remotelinks\]:"
	check_status
}

@test "reporter" {
	run grepdata "FIELD\[reporter\]: Watson Automation <watson-automation>"
	check_status
}

@test "resolution" {
	run grepdata "FIELD\[resolution\]: Not a Bug"
	check_status
}

@test "resolutiondate" {
	run grepdata "FIELD\[resolutiondate\]: 2024-09-03 12:22:34"
	check_status
}

@test "sprint" {
	run grepdata "FIELD\[sprint\]:"
	check_status
}

@test "status" {
	run grepdata "FIELD\[status\]: Closed"
	check_status
}

@test "subtasks" {
	run grepdata "FIELD\[subtasks\]:"
	check_status
}

@test "summary]: Update arch/x86 to 6.7 [rhel-9" {
	run grepdata "FIELD\[summary\]: Update arch/x86 to 6.7 \[rhel-9\]"
	check_status
}

@test "timeestimate" {
	run grepdata "FIELD\[timeestimate\]: 0"
	check_status
}

@test "timeoriginalestimate" {
	run grepdata "FIELD\[timeoriginalestimate\]: 0"
	check_status
}

@test "timespent" {
	run grepdata "FIELD\[timespent\]: 0"
	check_status
}

@test "timetracking" {
	run grepdata "FIELD\[timetracking\]: OriginalEstimate:  RemainingEstimate:  TimeSpent:  OriginalEstimateSeconds: 0 RemainingEstimateSeconds: 0 TimeSpentSeconds: 0"
	check_status
}

@test "watches" {
	run grepdata "FIELD\[watches\]:"
	check_status
}

@test "updated" {
	run grepdata "FIELD\[updated\]: 2024-09-03 12:22:34"
	check_status
}

@test "versions" {
	run grepdata "FIELD\[versions\]:"
	check_status
}

@test "Release Date | customfield_12322243" {
	run grepdata "FIELD\[Release Date | customfield_12322243\]:"
	check_status
}

@test "PX Impact Score | customfield_12322244" {
	run grepdata "FIELD\[PX Impact Score | customfield_12322244\]:"
	check_status
}

@test "SFDC Cases Open | customfield_12324540" {
	run grepdata "FIELD\[SFDC Cases Open | customfield_12324540\]: 0.0"
	check_status
}

@test "Staffing | customfield_12322240" {
	run grepdata "FIELD\[Staffing | customfield_12322240\]:"
	check_status
}

@test "Gating Tests | customfield_12322241" {
	run grepdata "FIELD\[Gating Tests | customfield_12322241\]:"
	check_status
}

@test "SVN / CVS Isolated Branch | customfield_12310022" {
	run grepdata "FIELD\[SVN / CVS Isolated Branch | customfield_12310022\]:"
	check_status
}

@test "Support Case Reference | customfield_12310021" {
	run grepdata "FIELD\[Support Case Reference | customfield_12310021\]:"
	check_status
}

@test "Affects Build | customfield_12312441" {
	run grepdata "FIELD\[Affects Build | customfield_12312441\]:"
	check_status
}

@test "Percent complete | customfield_12314741" {
	run grepdata "FIELD\[Percent complete | customfield_12314741\]:"
	check_status
}

@test "Fix Build | customfield_12312442" {
	run grepdata "FIELD\[Fix Build | customfield_12312442\]:"
	check_status
}

@test "Deployment Notes | customfield_12312440" {
	run grepdata "FIELD\[Deployment Notes | customfield_12312440\]:"
	check_status
}

@test "Contributors | customfield_12315950" {
	run grepdata "FIELD\[Contributors | customfield_12315950\]:"
	check_status
}

@test "Last Viewed | lastViewed" {
	run grepdata "FIELD\[Last Viewed | lastViewed\]: 2025-02-26T00:25:03.360+0000"
	check_status
}

@test "Internal Target Milestone | customfield_12321040" {
	run grepdata "FIELD\[Internal Target Milestone | customfield_12321040\]:"
	check_status
}

@test "Keywords | customfield_12323341" {
	run grepdata "FIELD\[Keywords | customfield_12323341\]: ZStream"
	check_status
}

@test "CDW qa_ack | customfield_12311244" {
	run grepdata "FIELD\[CDW qa_ack | customfield_12311244\]:"
	check_status
}

@test "CDW blocker | customfield_12311245" {
	run grepdata "FIELD\[CDW blocker | customfield_12311245\]:"
	check_status
}

@test "CDW pm_ack | customfield_12311242" {
	run grepdata "FIELD\[CDW pm_ack | customfield_12311242\]:"
	check_status
}

@test "PX Scheduling Request - OLD | customfield_12315841" {
	run grepdata "FIELD\[PX Scheduling Request - OLD | customfield_12315841\]:"
	check_status
}

@test "CDW devel_ack | customfield_12311243" {
	run grepdata "FIELD\[CDW devel_ack | customfield_12311243\]:"
	check_status
}

@test "Epic Type | customfield_12313541" {
	run grepdata "FIELD\[Epic Type | customfield_12313541\]:"
	check_status
}

@test "Target Release | customfield_12311240" {
	run grepdata "FIELD\[Target Release | customfield_12311240\]:"
	check_status
}

@test "CDW release | customfield_12311241" {
	run grepdata "FIELD\[CDW release | customfield_12311241\]:"
	check_status
}

@test "Affects | customfield_12310031" {
	run grepdata "FIELD\[Affects | customfield_12310031\]:"
	check_status
}

@test "CDW exception | customfield_12311246" {
	run grepdata "FIELD\[CDW exception | customfield_12311246\]:"
	check_status
}

@test "Escape Reason - OLD | customfield_12320041" {
	run grepdata "FIELD\[Escape Reason - OLD | customfield_12320041\]:"
	check_status
}

@test "QE Priority | customfield_12319294" {
	run grepdata "FIELD\[QE Priority | customfield_12319294\]:"
	check_status
}

@test "Affected Groups | customfield_12319293" {
	run grepdata "FIELD\[Affected Groups | customfield_12319293\]:"
	check_status
}

@test "CMDB ID | customfield_12324640" {
	run grepdata "FIELD\[CMDB ID | customfield_12324640\]:"
	check_status
}

@test "Work Type | customfield_12320040" {
	run grepdata "FIELD\[Work Type | customfield_12320040\]:"
	check_status
}

@test "ProdDocsReview-CCS | customfield_12322343" {
	run grepdata "FIELD\[ProdDocsReview-CCS | customfield_12322343\]:"
	check_status
}

@test "ProdDocsReview-QE | customfield_12322344" {
	run grepdata "FIELD\[ProdDocsReview-QE | customfield_12322344\]:"
	check_status
}

@test "Strategic Relationship | customfield_12319295" {
	run grepdata "FIELD\[Strategic Relationship | customfield_12319295\]:"
	check_status
}

@test "Corrective Measures-OLD | customfield_12320043" {
	run grepdata "FIELD\[Corrective Measures-OLD | customfield_12320043\]:"
	check_status
}

@test "Escape Impact - OLD | customfield_12320042" {
	run grepdata "FIELD\[Escape Impact - OLD | customfield_12320042\]:"
	check_status
}

@test "ProdDocsReview-Dev | customfield_12322340" {
	run grepdata "FIELD\[ProdDocsReview-Dev | customfield_12322340\]:"
	check_status
}

@test "SME | customfield_12319299" {
	run grepdata "FIELD\[SME | customfield_12319299\]:"
	check_status
}

@test "Training Opportunity | customfield_12319290" {
	run grepdata "FIELD\[Training Opportunity | customfield_12319290\]:"
	check_status
}

@test "Testing Instructions | customfield_12319292" {
	run grepdata "FIELD\[Testing Instructions | customfield_12319292\]:"
	check_status
}

@test "Training Opportunity Notes | customfield_12319291" {
	run grepdata "FIELD\[Training Opportunity Notes | customfield_12319291\]:"
	check_status
}

@test "Help Desk Ticket Reference | customfield_12310120" {
	run grepdata "FIELD\[Help Desk Ticket Reference | customfield_12310120\]:"
	check_status
}

@test "Doc Version/s | customfield_12314840" {
	run grepdata "FIELD\[Doc Version/s | customfield_12314840\]:"
	check_status
}

@test "Story Points | customfield_12310243" {
	run grepdata "FIELD\[Story Points | customfield_12310243\]:"
	check_status
}

@test "PX Technical Impact Notes | customfield_12325740" {
	run grepdata "FIELD\[PX Technical Impact Notes | customfield_12325740\]:"
	check_status
}

@test "GtmhubObjectID | customfield_12321140" {
	run grepdata "FIELD\[GtmhubObjectID | customfield_12321140\]:"
	check_status
}

@test "Onsite Date | customfield_12323440" {
	run grepdata "FIELD\[Onsite Date | customfield_12323440\]:"
	check_status
}

@test "Ready-Ready | customfield_12315943" {
	run grepdata "FIELD\[Ready-Ready | customfield_12315943\]:"
	check_status
}

@test "Forum Reference | customfield_12310010" {
	run grepdata "FIELD\[Forum Reference | customfield_12310010\]:"
	check_status
}

@test "Planned End | customfield_12315944" {
	run grepdata "FIELD\[Planned End | customfield_12315944\]:"
	check_status
}

@test "QA Contact | customfield_12315948" {
	run grepdata "FIELD\[QA Contact | customfield_12315948\]: rhn-support-wgomerin <wgomerin@redhat.com>"
	check_status
}

@test "Product | customfield_12315949" {
	run grepdata "FIELD\[Product | customfield_12315949\]:"
	check_status
}

@test "Votes | votes" {
	run grepdata "FIELD\[Votes | votes\]: 0.000000"
	check_status
}

@test "Acceptance Criteria | customfield_12315940" {
	run grepdata "FIELD\[Acceptance Criteria | customfield_12315940\]:"
	check_status
}

@test "Backlogs | customfield_12315941" {
	run grepdata "FIELD\[Backlogs | customfield_12315941\]:"
	check_status
}

@test "Escape Impact | customfield_12324344" {
	run grepdata "FIELD\[Escape Impact | customfield_12324344\]:"
	check_status
}

@test "SDLC stage when should've been found | customfield_12324343" {
	run grepdata "FIELD\[SDLC stage when should've been found | customfield_12324343\]:"
	check_status
}

@test "Corrective Measures | customfield_12324345" {
	run grepdata "FIELD\[Corrective Measures | customfield_12324345\]:"
	check_status
}

@test "Escape Reason | customfield_12324340" {
	run grepdata "FIELD\[Escape Reason | customfield_12324340\]:"
	check_status
}

@test "Internal Whiteboard | customfield_12322040" {
	run grepdata "FIELD\[Internal Whiteboard | customfield_12322040\]:"
	check_status
}

@test "SDLC stage when introduced | customfield_12324342" {
	run grepdata "FIELD\[SDLC stage when introduced | customfield_12324342\]:"
	check_status
}

@test "SDLC stage when found | customfield_12324341" {
	run grepdata "FIELD\[SDLC stage when found | customfield_12324341\]:"
	check_status
}

@test "Engineering Response | customfield_12310181" {
	run grepdata "FIELD\[Engineering Response | customfield_12310181\]:"
	check_status
}

@test "Patch Visibility | customfield_12310060" {
	run grepdata "FIELD\[Patch Visibility | customfield_12310060\]:"
	check_status
}

@test "Background and Context | customfield_12310180" {
	run grepdata "FIELD\[Background and Context | customfield_12310180\]:"
	check_status
}

@test "5-Acks Check | customfield_12316845" {
	run grepdata "FIELD\[5-Acks Check | customfield_12316845\]:"
	check_status
}

@test "GitHub Issue | customfield_12316846" {
	run grepdata "FIELD\[GitHub Issue | customfield_12316846\]:"
	check_status
}

@test "Developer Comment | customfield_12316847" {
	run grepdata "FIELD\[Developer Comment | customfield_12316847\]:"
	check_status
}

@test "ODC Planning | customfield_12316848" {
	run grepdata "FIELD\[ODC Planning | customfield_12316848\]:"
	check_status
}

@test "ODC Planning Ack | customfield_12316849" {
	run grepdata "FIELD\[ODC Planning Ack | customfield_12316849\]:"
	check_status
}

@test "Steps to Reproduce | customfield_12310183" {
	run grepdata "FIELD\[Steps to Reproduce | customfield_12310183\]:"
	check_status
}

@test "Product Management Response | customfield_12310182" {
	run grepdata "FIELD\[Product Management Response | customfield_12310182\]:"
	check_status
}

@test "QE Status | customfield_12312240" {
	run grepdata "FIELD\[QE Status | customfield_12312240\]:"
	check_status
}

@test "Bugzilla Bug | customfield_12316840" {
	run grepdata "FIELD\[Bugzilla Bug | customfield_12316840\]:"
	check_status
}

@test "Sync Failure Flag | customfield_12316841" {
	run grepdata "FIELD\[Sync Failure Flag | customfield_12316841\]:"
	check_status
}

@test "Sync Failure Message | customfield_12316842" {
	run grepdata "FIELD\[Sync Failure Message | customfield_12316842\]:"
	check_status
}

@test "Whiteboard | customfield_12316843" {
	run grepdata "FIELD\[Whiteboard | customfield_12316843\]:"
	check_status
}

@test "Time to resolution | customfield_12325441" {
	run grepdata "FIELD\[Time to resolution | customfield_12325441\]:"
	check_status
}

@test "Target Version - old | customfield_12323140" {
	run grepdata "FIELD\[Target Version - old | customfield_12323140\]:"
	check_status
}

@test "Time to first response | customfield_12325442" {
	run grepdata "FIELD\[Time to first response | customfield_12325442\]:"
	check_status
}

@test "Approvers | customfield_12325440" {
	run grepdata "FIELD\[Approvers | customfield_12325440\]:"
	check_status
}

@test "Patch Repository Link | customfield_12310071" {
	run grepdata "FIELD\[Patch Repository Link | customfield_12310071\]:"
	check_status
}

@test "Patch Instructions | customfield_12310070" {
	run grepdata "FIELD\[Patch Instructions | customfield_12310070\]:"
	check_status
}

@test "Doc Required | customfield_12315640" {
	run grepdata "FIELD\[Doc Required | customfield_12315640\]:"
	check_status
}

@test "Stackoverflow ID | customfield_12313340" {
	run grepdata "FIELD\[Stackoverflow ID | customfield_12313340\]:"
	check_status
}

@test "Service Delivery Planning ACK | customfield_12316850" {
	run grepdata "FIELD\[Service Delivery Planning ACK | customfield_12316850\]:"
	check_status
}

@test "Funding State | customfield_12317940" {
	run grepdata "FIELD\[Funding State | customfield_12317940\]:"
	check_status
}

@test "Funding Strategy | customfield_12317941" {
	run grepdata "FIELD\[Funding Strategy | customfield_12317941\]:"
	check_status
}

@test "Jira Link | customfield_12324443" {
	run grepdata "FIELD\[Jira Link | customfield_12324443\]:"
	check_status
}

@test "PX Impact Range | customfield_12322143" {
	run grepdata "FIELD\[PX Impact Range | customfield_12322143\]:"
	check_status
}

@test "Other Savings | customfield_12324442" {
	run grepdata "FIELD\[Other Savings | customfield_12324442\]:"
	check_status
}

@test "Risk Identified Date | customfield_12322144" {
	run grepdata "FIELD\[Risk Identified Date | customfield_12322144\]:"
	check_status
}

@test "Additional Approvers | customfield_12322145" {
	run grepdata "FIELD\[Additional Approvers | customfield_12322145\]:"
	check_status
}

@test "Security Approvals | customfield_12322146" {
	run grepdata "FIELD\[Security Approvals | customfield_12322146\]:"
	check_status
}

@test "PX Review Complete | customfield_12322140" {
	run grepdata "FIELD\[PX Review Complete | customfield_12322140\]:"
	check_status
}

@test "Annual Cost Savings/Avoidance | customfield_12324441" {
	run grepdata "FIELD\[Annual Cost Savings/Avoidance | customfield_12324441\]:"
	check_status
}

@test "PX Technical Impact | customfield_12322141" {
	run grepdata "FIELD\[PX Technical Impact | customfield_12322141\]:"
	check_status
}

@test "Annual Time Savings | customfield_12324440" {
	run grepdata "FIELD\[Annual Time Savings | customfield_12324440\]:"
	check_status
}

@test "PX Priority Data | customfield_12322142" {
	run grepdata "FIELD\[PX Priority Data | customfield_12322142\]:"
	check_status
}

@test "Dev Target end | customfield_12322148" {
	run grepdata "FIELD\[Dev Target end | customfield_12322148\]:"
	check_status
}

@test "GSS Priority | customfield_12312340" {
	run grepdata "FIELD\[GSS Priority | customfield_12312340\]:"
	check_status
}

@test "Upstream Jira | customfield_12314640" {
	run grepdata "FIELD\[Upstream Jira | customfield_12314640\]:"
	check_status
}

@test "Customer Name | customfield_12310160" {
	run grepdata "FIELD\[Customer Name | customfield_12310160\]:"
	check_status
}

@test "Team Confidence | customfield_12316940" {
	run grepdata "FIELD\[Team Confidence | customfield_12316940\]:"
	check_status
}

@test "QE Confidence | customfield_12316941" {
	run grepdata "FIELD\[QE Confidence | customfield_12316941\]:"
	check_status
}

@test "Doc Confidence | customfield_12316942" {
	run grepdata "FIELD\[Doc Confidence | customfield_12316942\]:"
	check_status
}

@test "Business Value | customfield_12316943" {
	run grepdata "FIELD\[Business Value | customfield_12316943\]:"
	check_status
}

@test "Contributing Groups Limited Access | customfield_12323243" {
	run grepdata "FIELD\[Contributing Groups Limited Access | customfield_12323243\]:"
	check_status
}

@test "Anomaly | customfield_12323240" {
	run grepdata "FIELD\[Anomaly | customfield_12323240\]:"
	check_status
}

@test "Exception Count | customfield_12322150" {
	run grepdata "FIELD\[Exception Count | customfield_12322150\]:"
	check_status
}

@test "Security Controls | customfield_12322151" {
	run grepdata "FIELD\[Security Controls | customfield_12322151\]:"
	check_status
}

@test "Blocked by Bugzilla Bug | customfield_12322152" {
	run grepdata "FIELD\[Blocked by Bugzilla Bug | customfield_12322152\]:"
	check_status
}

@test "Request Clones | customfield_12323242" {
	run grepdata "FIELD\[Request Clones | customfield_12323242\]:"
	check_status
}

@test "Anomaly Criticality | customfield_12323241" {
	run grepdata "FIELD\[Anomaly Criticality | customfield_12323241\]:"
	check_status
}

@test "Affects Testing | customfield_12310170" {
	run grepdata "FIELD\[Affects Testing | customfield_12310170\]:"
	check_status
}

@test "Affected Jars | customfield_12313443" {
	run grepdata "FIELD\[Affected Jars | customfield_12313443\]:"
	check_status
}

@test "Epic Colour | customfield_12311143" {
	run grepdata "FIELD\[Epic Colour | customfield_12311143\]:"
	check_status
}

@test "PDD Priority | customfield_12313442" {
	run grepdata "FIELD\[PDD Priority | customfield_12313442\]:"
	check_status
}

@test "Epic Name | customfield_12311141" {
	run grepdata "FIELD\[Epic Name | customfield_12311141\]:"
	check_status
}

@test "SFDC Cases Links | customfield_12313441" {
	run grepdata "FIELD\[SFDC Cases Links | customfield_12313441\]:"
	check_status
}

@test "issueFunction | customfield_12315740" {
	run grepdata "FIELD\[issueFunction | customfield_12315740\]:"
	check_status
}

@test "Epic Status | customfield_12311142" {
	run grepdata "FIELD\[Epic Status | customfield_12311142\]:"
	check_status
}

@test "SFDC Cases Counter | customfield_12313440" {
	run grepdata "FIELD\[SFDC Cases Counter | customfield_12313440\]: 0.0"
	check_status
}

@test "Component Fix Version(s) | customfield_12310173" {
	run grepdata "FIELD\[Component Fix Version(s) | customfield_12310173\]:"
	check_status
}

@test "Epic Link | customfield_12311140" {
	run grepdata "FIELD\[Epic Link | customfield_12311140\]:"
	check_status
}

@test "cee_cir | customfield_12314340" {
	run grepdata "FIELD\[cee_cir | customfield_12314340\]:"
	check_status
}

@test "Work Category | customfield_12324140" {
	run grepdata "FIELD\[Work Category | customfield_12324140\]:"
	check_status
}

@test "Request participants | customfield_12325242" {
	run grepdata "FIELD\[Request participants | customfield_12325242\]:"
	check_status
}

@test "Approvals | customfield_12325241" {
	run grepdata "FIELD\[Approvals | customfield_12325241\]:"
	check_status
}

@test "Satisfaction | customfield_12325243" {
	run grepdata "FIELD\[Satisfaction | customfield_12325243\]:"
	check_status
}

@test "Parent Link | customfield_12313140" {
	run grepdata "FIELD\[Parent Link | customfield_12313140\]:"
	check_status
}

@test "QE Estimate | customfield_12313145" {
	run grepdata "FIELD\[QE Estimate | customfield_12313145\]:"
	check_status
}

@test "EAP PT Community Docs (CD) | customfield_12313143" {
	run grepdata "FIELD\[EAP PT Community Docs (CD) | customfield_12313143\]:"
	check_status
}

@test "EAP PT Test Dev (TD) | customfield_12313144" {
	run grepdata "FIELD\[EAP PT Test Dev (TD) | customfield_12313144\]:"
	check_status
}

@test "Planning Status | customfield_12317740" {
	run grepdata "FIELD\[Planning Status | customfield_12317740\]:"
	check_status
}

@test "Linked major incidents | customfield_12325240" {
	run grepdata "FIELD\[Linked major incidents | customfield_12325240\]:"
	check_status
}

@test "SRE Contact | customfield_12324240" {
	run grepdata "FIELD\[SRE Contact | customfield_12324240\]:"
	check_status
}

@test "Tester | customfield_12310080" {
	run grepdata "FIELD\[Tester | customfield_12310080\]:"
	check_status
}

@test "Archived | archiveddate" {
	run grepdata "FIELD\[Archived | archiveddate\]:"
	check_status
}

@test "Account Number | customfield_12316747" {
	run grepdata "FIELD\[Account Number | customfield_12316747\]:"
	check_status
}

@test "Architect | customfield_12316749" {
	run grepdata "FIELD\[Architect | customfield_12316749\]:"
	check_status
}

@test "Class of work | customfield_12312142" {
	run grepdata "FIELD\[Class of work | customfield_12312142\]:"
	check_status
}

@test "Prod build version | customfield_12316740" {
	run grepdata "FIELD\[Prod build version | customfield_12316740\]:"
	check_status
}

@test "QE ACK | customfield_12316745" {
	run grepdata "FIELD\[QE ACK | customfield_12316745\]:"
	check_status
}

@test "Satisfaction date | customfield_12325342" {
	run grepdata "FIELD\[Satisfaction date | customfield_12325342\]:"
	check_status
}

@test "Groups | customfield_12325343" {
	run grepdata "FIELD\[Groups | customfield_12325343\]:"
	check_status
}

@test "Customer Request Type | customfield_12325340" {
	run grepdata "FIELD\[Customer Request Type | customfield_12325340\]:"
	check_status
}

@test "Organizations | customfield_12325341" {
	run grepdata "FIELD\[Organizations | customfield_12325341\]:"
	check_status
}

@test "Workaround Description | customfield_12310091" {
	run grepdata "FIELD\[Workaround Description | customfield_12310091\]:"
	check_status
}

@test "Workaround | customfield_12310090" {
	run grepdata "FIELD\[Workaround | customfield_12310090\]:"
	check_status
}

@test "Estimated Difficulty | customfield_12310092" {
	run grepdata "FIELD\[Estimated Difficulty | customfield_12310092\]:"
	check_status
}

@test "Flagged | customfield_12315542" {
	run grepdata "FIELD\[Flagged | customfield_12315542\]:"
	check_status
}

@test "Regression Test | customfield_12315541" {
	run grepdata "FIELD\[Regression Test | customfield_12315541\]:"
	check_status
}

@test "EAP Docs SME | customfield_12315540" {
	run grepdata "FIELD\[EAP Docs SME | customfield_12315540\]:"
	check_status
}

@test "Team | customfield_12313240" {
	run grepdata "FIELD\[Team | customfield_12313240\]:"
	check_status
}

@test "Technical Lead | customfield_12316750" {
	run grepdata "FIELD\[Technical Lead | customfield_12316750\]:"
	check_status
}

@test "Designer | customfield_12316751" {
	run grepdata "FIELD\[Designer | customfield_12316751\]:"
	check_status
}

@test "PX Scheduling Request | customfield_12323040" {
	run grepdata "FIELD\[PX Scheduling Request | customfield_12323040\]:"
	check_status
}

@test "Fuse Progress Bar | customfield_12317841" {
	run grepdata "FIELD\[Fuse Progress Bar | customfield_12317841\]:"
	check_status
}

@test "Product Manager | customfield_12316752" {
	run grepdata "FIELD\[Product Manager | customfield_12316752\]:"
	check_status
}

@test "Fuse Progress | customfield_12317842" {
	run grepdata "FIELD\[Fuse Progress | customfield_12317842\]:"
	check_status
}

@test "Manager | customfield_12316753" {
	run grepdata "FIELD\[Manager | customfield_12316753\]:"
	check_status
}

@test "BZ Partner | customfield_12317843" {
	run grepdata "FIELD\[BZ Partner | customfield_12317843\]:"
	check_status
}

@test "Test Coverage | customfield_12320940" {
	run grepdata "FIELD\[Test Coverage | customfield_12320940\]:"
	check_status
}

@test "Test Link-Old | customfield_12320944" {
	run grepdata "FIELD\[Test Link-Old | customfield_12320944\]:"
	check_status
}

@test "Pervasiveness | customfield_12320943" {
	run grepdata "FIELD\[Pervasiveness | customfield_12320943\]:"
	check_status
}

@test "Willingness to Pay | customfield_12320941" {
	run grepdata "FIELD\[Willingness to Pay | customfield_12320941\]:"
	check_status
}

@test "Work Source | customfield_12316441" {
	run grepdata "FIELD\[Work Source | customfield_12316441\]:"
	check_status
}

@test "Work Ratio | workratio" {
	run grepdata "FIELD\[Work Ratio | workratio\]: -1.000000"
	check_status
}

@test "Customer Cloud Subscription (CCS) | customfield_12316442" {
	run grepdata "FIELD\[Customer Cloud Subscription (CCS) | customfield_12316442\]:"
	check_status
}

@test "Portfolio Solutions | customfield_12318740" {
	run grepdata "FIELD\[Portfolio Solutions | customfield_12318740\]:"
	check_status
}

@test "Cluster Admin Enabled | customfield_12316443" {
	run grepdata "FIELD\[Cluster Admin Enabled | customfield_12316443\]:"
	check_status
}

@test "Cloud Platform | customfield_12316444" {
	run grepdata "FIELD\[Cloud Platform | customfield_12316444\]:"
	check_status
}

@test "Support Scope | customfield_12317540" {
	run grepdata "FIELD\[Support Scope | customfield_12317540\]:"
	check_status
}

@test "EAP Testing By | customfield_12315240" {
	run grepdata "FIELD\[EAP Testing By | customfield_12315240\]:"
	check_status
}

@test "Experience | customfield_12320948" {
	run grepdata "FIELD\[Experience | customfield_12320948\]: Needs Review"
	check_status
}

@test "Original Estimate | customfield_12317307" {
	run grepdata "FIELD\[Original Estimate | customfield_12317307\]:"
	check_status
}

@test "Market | customfield_12320947" {
	run grepdata "FIELD\[Market | customfield_12320947\]: Unclassified"
	check_status
}

@test "BZ URL | customfield_12317309" {
	run grepdata "FIELD\[BZ URL | customfield_12317309\]:"
	check_status
}

@test "Intelligence Requested | customfield_12320946" {
	run grepdata "FIELD\[Intelligence Requested | customfield_12320946\]:"
	check_status
}

@test "Risk Category | customfield_12320945" {
	run grepdata "FIELD\[Risk Category | customfield_12320945\]:"
	check_status
}

@test "External issue ID | customfield_12325040" {
	run grepdata "FIELD\[External issue ID | customfield_12325040\]:"
	check_status
}

@test "External issue ID | customfield_12319840" {
	run grepdata "FIELD\[External issue ID | customfield_12319840\]:"
	check_status
}

@test "Risk Type | customfield_12317301" {
	run grepdata "FIELD\[Risk Type | customfield_12317301\]:"
	check_status
}

@test "Risk Probability | customfield_12317302" {
	run grepdata "FIELD\[Risk Probability | customfield_12317302\]:"
	check_status
}

@test "Risk Proximity | customfield_12317303" {
	run grepdata "FIELD\[Risk Proximity | customfield_12317303\]:"
	check_status
}

@test "Risk Impact Level | customfield_12317304" {
	run grepdata "FIELD\[Risk Impact Level | customfield_12317304\]:"
	check_status
}

@test "Risk impact description | customfield_12317305" {
	run grepdata "FIELD\[Risk impact description | customfield_12317305\]:"
	check_status
}

@test "Risk mitigation/contingency | customfield_12317306" {
	run grepdata "FIELD\[Risk mitigation/contingency | customfield_12317306\]:"
	check_status
}

@test "Operating System_Lab | customfield_12324043" {
	run grepdata "FIELD\[Operating System_Lab | customfield_12324043\]:"
	check_status
}

@test "Actual Effort | customfield_12316548" {
	run grepdata "FIELD\[Actual Effort | customfield_12316548\]:"
	check_status
}

@test "Chapter | customfield_12316549" {
	run grepdata "FIELD\[Chapter | customfield_12316549\]:"
	check_status
}

@test "EAP PT Pre-Checked (PC) | customfield_12314245" {
	run grepdata "FIELD\[EAP PT Pre-Checked (PC) | customfield_12314245\]:"
	check_status
}

@test "EAP PT Product Docs (PD) | customfield_12314244" {
	run grepdata "FIELD\[EAP PT Product Docs (PD) | customfield_12314244\]:"
	check_status
}

@test "EAP PT Docs Analysis (DA) | customfield_12314243" {
	run grepdata "FIELD\[EAP PT Docs Analysis (DA) | customfield_12314243\]:"
	check_status
}

@test "EAP PT Test Plan (TP) | customfield_12314242" {
	run grepdata "FIELD\[EAP PT Test Plan (TP) | customfield_12314242\]:"
	check_status
}

@test "EAP PT Analysis Document (AD) | customfield_12314241" {
	run grepdata "FIELD\[EAP PT Analysis Document (AD) | customfield_12314241\]:"
	check_status
}

@test "Marketing Info | customfield_12318840" {
	run grepdata "FIELD\[Marketing Info | customfield_12318840\]:"
	check_status
}

@test "Reviewer | customfield_12316540" {
	run grepdata "FIELD\[Reviewer | customfield_12316540\]:"
	check_status
}

@test "T-Shirt Size | customfield_12316541" {
	run grepdata "FIELD\[T-Shirt Size | customfield_12316541\]:"
	check_status
}

@test "Commit Hashes | customfield_12324041" {
	run grepdata "FIELD\[Commit Hashes | customfield_12324041\]:"
	check_status
}

@test "Ready | customfield_12316542" {
	run grepdata "FIELD\[Ready | customfield_12316542\]: False"
	check_status
}

@test "Blocked | customfield_12316543" {
	run grepdata "FIELD\[Blocked | customfield_12316543\]: False"
	check_status
}

@test "Marketing Info Ready | customfield_12318841" {
	run grepdata "FIELD\[Marketing Info Ready | customfield_12318841\]:"
	check_status
}

@test "Product Documentation Required | customfield_12324040" {
	run grepdata "FIELD\[Product Documentation Required | customfield_12324040\]:"
	check_status
}

@test "Blocked Reason | customfield_12316544" {
	run grepdata "FIELD\[Blocked Reason | customfield_12316544\]: None"
	check_status
}

@test "Owner | customfield_12316545" {
	run grepdata "FIELD\[Owner | customfield_12316545\]:"
	check_status
}

@test "Evidence Score | customfield_12316546" {
	run grepdata "FIELD\[Evidence Score | customfield_12316546\]:"
	check_status
}

@test "Discussed with Team | customfield_12316547" {
	run grepdata "FIELD\[Discussed with Team | customfield_12316547\]:"
	check_status
}

@test "Test Plan | customfield_12313040" {
	run grepdata "FIELD\[Test Plan | customfield_12313040\]:"
	check_status
}

@test "Analysis Document | customfield_12313041" {
	run grepdata "FIELD\[Analysis Document | customfield_12313041\]:"
	check_status
}

@test "Microrelease version | customfield_12313042" {
	run grepdata "FIELD\[Microrelease version | customfield_12313042\]:"
	check_status
}

@test "DevTestDoc | customfield_12317640" {
	run grepdata "FIELD\[DevTestDoc | customfield_12317640\]:"
	check_status
}

@test "Target Version | customfield_12319940" {
	run grepdata "FIELD\[Target Version | customfield_12319940\]:"
	check_status
}

@test "Functional Safety Justification | customfield_12325141" {
	run grepdata "FIELD\[Functional Safety Justification | customfield_12325141\]:"
	check_status
}

@test "EXD-Service | customfield_12317401" {
	run grepdata "FIELD\[EXD-Service | customfield_12317401\]:"
	check_status
}

@test "Functional Safety Triage | customfield_12325140" {
	run grepdata "FIELD\[Functional Safety Triage | customfield_12325140\]:"
	check_status
}

@test "EXD-WorkType | customfield_12317403" {
	run grepdata "FIELD\[EXD-WorkType | customfield_12317403\]:"
	check_status
}

@test "Commitment | customfield_12317404" {
	run grepdata "FIELD\[Commitment | customfield_12317404\]:"
	check_status
}

@test "Requesting Teams | customfield_12317405" {
	run grepdata "FIELD\[Requesting Teams | customfield_12317405\]:"
	check_status
}

@test "Planning | customfield_12316240" {
	run grepdata "FIELD\[Planning | customfield_12316240\]:"
	check_status
}

@test "BZ needinfo | customfield_12317330" {
	run grepdata "FIELD\[BZ needinfo | customfield_12317330\]:"
	check_status
}

@test "Design Doc | customfield_12316241" {
	run grepdata "FIELD\[Design Doc | customfield_12316241\]:"
	check_status
}

@test "Risk Score | customfield_12319751" {
	run grepdata "FIELD\[Risk Score | customfield_12319751\]:"
	check_status
}

@test "BZ rhel-8.0.z | customfield_12317331" {
	run grepdata "FIELD\[BZ rhel-8.0.z | customfield_12317331\]:"
	check_status
}

@test "Message For OHSS Project | customfield_12318540" {
	run grepdata "FIELD\[Message For OHSS Project | customfield_12318540\]:"
	check_status
}

@test "QEStatus | customfield_12316242" {
	run grepdata "FIELD\[QEStatus | customfield_12316242\]:"
	check_status
}

@test "WSJF | customfield_12319750" {
	run grepdata "FIELD\[WSJF | customfield_12319750\]:"
	check_status
}

@test "Urgency | customfield_12320741" {
	run grepdata "FIELD\[Urgency | customfield_12320741\]:"
	check_status
}

@test "Impact | customfield_12320740" {
	run grepdata "FIELD\[Impact | customfield_12320740\]:"
	check_status
}

@test "ZStream Target Release | customfield_12317332" {
	run grepdata "FIELD\[ZStream Target Release | customfield_12317332\]:"
	check_status
}

@test "BZ blocker | customfield_12317333" {
	run grepdata "FIELD\[BZ blocker | customfield_12317333\]:"
	check_status
}

@test "Needs Product Docs | customfield_12316245" {
	run grepdata "FIELD\[Needs Product Docs | customfield_12316245\]:"
	check_status
}

@test "zstream | customfield_12317334" {
	run grepdata "FIELD\[zstream | customfield_12317334\]:"
	check_status
}

@test "BZ Version | customfield_12317335" {
	run grepdata "FIELD\[BZ Version | customfield_12317335\]:"
	check_status
}

@test "BZ Docs Contact | customfield_12317336" {
	run grepdata "FIELD\[BZ Docs Contact | customfield_12317336\]:"
	check_status
}

@test "BZ requires_doc_text | customfield_12317337" {
	run grepdata "FIELD\[BZ requires_doc_text | customfield_12317337\]:"
	check_status
}

@test "Doc Commitment | customfield_12317338" {
	run grepdata "FIELD\[Doc Commitment | customfield_12317338\]:"
	check_status
}

@test "Sprint | customfield_12310940" {
	run grepdata "FIELD\[Sprint | customfield_12310940\]:"
	check_status
}

@test "Link to documentation | customfield_12316250" {
	run grepdata "FIELD\[Link to documentation | customfield_12316250\]:"
	check_status
}

@test "OS | customfield_12317340" {
	run grepdata "FIELD\[OS | customfield_12317340\]:"
	check_status
}

@test "Public Target Launch Date | customfield_12317341" {
	run grepdata "FIELD\[Public Target Launch Date | customfield_12317341\]:"
	check_status
}

@test "Contributing Groups | customfield_12319640" {
	run grepdata "FIELD\[Contributing Groups | customfield_12319640\]:"
	check_status
}

@test "Onsite Hardware Date - old | customfield_12317342" {
	run grepdata "FIELD\[Onsite Hardware Date - old | customfield_12317342\]:"
	check_status
}

@test "3scale PT Verified Product | customfield_12315043" {
	run grepdata "FIELD\[3scale PT Verified Product | customfield_12315043\]:"
	check_status
}

@test "3scale PT Released In Saas | customfield_12315042" {
	run grepdata "FIELD\[3scale PT Released In Saas | customfield_12315042\]:"
	check_status
}

@test "3scale PT Docs | customfield_12315041" {
	run grepdata "FIELD\[3scale PT Docs | customfield_12315041\]: Not Started"
	check_status
}

@test "3scale PT Product Specs | customfield_12315040" {
	run grepdata "FIELD\[3scale PT Product Specs | customfield_12315040\]:"
	check_status
}

@test "Supply Chain STI | customfield_12321840" {
	run grepdata "FIELD\[Supply Chain STI | customfield_12321840\]:"
	check_status
}

@test "3scale PT Product Update Ready | customfield_12315044" {
	run grepdata "FIELD\[3scale PT Product Update Ready | customfield_12315044\]:"
	check_status
}

@test "Target Upstream Version | customfield_12317343" {
	run grepdata "FIELD\[Target Upstream Version | customfield_12317343\]:"
	check_status
}

@test "Close Duplicate Candidate | customfield_12317344" {
	run grepdata "FIELD\[Close Duplicate Candidate | customfield_12317344\]:"
	check_status
}

@test "Partner Requirement State | customfield_12317345" {
	run grepdata "FIELD\[Partner Requirement State | customfield_12317345\]:"
	check_status
}

@test "BZ Target Release | customfield_12317346" {
	run grepdata "FIELD\[BZ Target Release | customfield_12317346\]:"
	check_status
}

@test "Upstream Kernel Target | customfield_12317347" {
	run grepdata "FIELD\[Upstream Kernel Target | customfield_12317347\]:"
	check_status
}

@test "EPM priority | customfield_12317348" {
	run grepdata "FIELD\[EPM priority | customfield_12317348\]:"
	check_status
}

@test "Case Link | customfield_12317349" {
	run grepdata "FIELD\[Case Link | customfield_12317349\]:"
	check_status
}

@test "BZ Flags | customfield_12318640" {
	run grepdata "FIELD\[BZ Flags | customfield_12318640\]:"
	check_status
}

@test "Service / (sub)product | customfield_12316340" {
	run grepdata "FIELD\[Service / (sub)product | customfield_12316340\]:"
	check_status
}

@test "Department | customfield_12316341" {
	run grepdata "FIELD\[Department | customfield_12316341\]:"
	check_status
}

@test "Status Summary | customfield_12320841" {
	run grepdata "FIELD\[Status Summary | customfield_12320841\]:"
	check_status
}

@test "Release Type | customfield_12320840" {
	run grepdata "FIELD\[Release Type | customfield_12320840\]:"
	check_status
}

@test "Original story points | customfield_12314040" {
	run grepdata "FIELD\[Original story points | customfield_12314040\]:"
	check_status
}

@test "Color Status | customfield_12320845" {
	run grepdata "FIELD\[Color Status | customfield_12320845\]:"
	check_status
}

@test "Customer Impact | customfield_12320844" {
	run grepdata "FIELD\[Customer Impact | customfield_12320844\]:"
	check_status
}

@test "UX or UI Contact | customfield_12320843" {
	run grepdata "FIELD\[UX or UI Contact | customfield_12320843\]:"
	check_status
}

@test "Documentation Type | customfield_12320842" {
	run grepdata "FIELD\[Documentation Type | customfield_12320842\]:"
	check_status
}

@test "BZ Keywords | customfield_12317318" {
	run grepdata "FIELD\[BZ Keywords | customfield_12317318\]:"
	check_status
}

@test "BZ exception | customfield_12317319" {
	run grepdata "FIELD\[BZ exception | customfield_12317319\]:"
	check_status
}

@test "Additional Assignees | customfield_12316342" {
	run grepdata "FIELD\[Additional Assignees | customfield_12316342\]:"
	check_status
}

@test "BZ Doc Type | customfield_12317310" {
	run grepdata "FIELD\[BZ Doc Type | customfield_12317310\]:"
	check_status
}

@test "BZ QA Whiteboard | customfield_12317311" {
	run grepdata "FIELD\[BZ QA Whiteboard | customfield_12317311\]:"
	check_status
}

@test "OpenShift Planning Ack | customfield_12316343" {
	run grepdata "FIELD\[OpenShift Planning Ack | customfield_12316343\]:"
	check_status
}

@test "BZ Internal Whiteboard | customfield_12317312" {
	run grepdata "FIELD\[BZ Internal Whiteboard | customfield_12317312\]:"
	check_status
}

@test "Release Note Text | customfield_12317313" {
	run grepdata "FIELD\[Release Note Text | customfield_12317313\]:"
	check_status
}

@test "Quarter | customfield_12317314" {
	run grepdata "FIELD\[Quarter | customfield_12317314\]:"
	check_status
}

@test "release | customfield_12317315" {
	run grepdata "FIELD\[release | customfield_12317315\]:"
	check_status
}

@test "Architecture | customfield_12316348" {
	run grepdata "FIELD\[Architecture | customfield_12316348\]:"
	check_status
}

@test "BZ Target Milestone | customfield_12317317" {
	run grepdata "FIELD\[BZ Target Milestone | customfield_12317317\]:"
	check_status
}

@test "Cluster ID | customfield_12316349" {
	run grepdata "FIELD\[Cluster ID | customfield_12316349\]:"
	check_status
}

@test "CRT Acceptance Critera | customfield_12316350" {
	run grepdata "FIELD\[CRT Acceptance Critera | customfield_12316350\]:"
	check_status
}

@test "Aha! URL | customfield_12317440" {
	run grepdata "FIELD\[Aha! URL | customfield_12317440\]:"
	check_status
}

@test "Approved | customfield_12319740" {
	run grepdata "FIELD\[Approved | customfield_12319740\]:"
	check_status
}

@test "Current Status | customfield_12317320" {
	run grepdata "FIELD\[Current Status | customfield_12317320\]:"
	check_status
}

@test "Satellite Team | customfield_12317441" {
	run grepdata "FIELD\[Satellite Team | customfield_12317441\]:"
	check_status
}

@test "Size | customfield_12320852" {
	run grepdata "FIELD\[Size | customfield_12320852\]:"
	check_status
}

@test "Developer | customfield_12315141" {
	run grepdata "FIELD\[Developer | customfield_12315141\]: arch-hw-x86-triage <arch-hw-x86-triage@redhat.com>"
	check_status
}

@test "Sub-System Group | customfield_12320851" {
	run grepdata "FIELD\[Sub-System Group | customfield_12320851\]: ssg_platform_enablement"
	check_status
}

@test "3Scale PT Tested upstream | customfield_12315140" {
	run grepdata "FIELD\[3Scale PT Tested upstream | customfield_12315140\]:"
	check_status
}

@test "Release Note Type | customfield_12320850" {
	run grepdata "FIELD\[Release Note Type | customfield_12320850\]:"
	check_status
}

@test "Supply Chain Program | customfield_12321940" {
	run grepdata "FIELD\[Supply Chain Program | customfield_12321940\]:"
	check_status
}

@test "PM Score | customfield_12317329" {
	run grepdata "FIELD\[PM Score | customfield_12317329\]:"
	check_status
}

@test "RICE Score | customfield_12320849" {
	run grepdata "FIELD\[RICE Score | customfield_12320849\]:"
	check_status
}

@test "Cost of Delay | customfield_12319749" {
	run grepdata "FIELD\[Cost of Delay | customfield_12319749\]:"
	check_status
}

@test "Effort | customfield_12320848" {
	run grepdata "FIELD\[Effort | customfield_12320848\]:"
	check_status
}

@test "Confidence | customfield_12320847" {
	run grepdata "FIELD\[Confidence | customfield_12320847\]:"
	check_status
}

@test "Reach | customfield_12320846" {
	run grepdata "FIELD\[Reach | customfield_12320846\]:"
	check_status
}

@test "BZ Assignee | customfield_12317321" {
	run grepdata "FIELD\[BZ Assignee | customfield_12317321\]:"
	check_status
}

@test "Release Commit Exception | customfield_12319742" {
	run grepdata "FIELD\[Release Commit Exception | customfield_12319742\]:"
	check_status
}

@test "Upstream Discussion | customfield_12317442" {
	run grepdata "FIELD\[Upstream Discussion | customfield_12317442\]:"
	check_status
}

@test "BZ Doc Text | customfield_12317322" {
	run grepdata "FIELD\[BZ Doc Text | customfield_12317322\]:"
	check_status
}

@test "Market Intelligence Score | customfield_12319741" {
	run grepdata "FIELD\[Market Intelligence Score | customfield_12319741\]:"
	check_status
}

@test "Responsible | customfield_12319744" {
	run grepdata "FIELD\[Responsible | customfield_12319744\]:"
	check_status
}

@test "BZ Product | customfield_12317324" {
	run grepdata "FIELD\[BZ Product | customfield_12317324\]:"
	check_status
}

@test "Release Blocker | customfield_12319743" {
	run grepdata "FIELD\[Release Blocker | customfield_12319743\]:"
	check_status
}

@test "Risk Response | customfield_12319746" {
	run grepdata "FIELD\[Risk Response | customfield_12319746\]:"
	check_status
}

@test "Archiver | archivedby" {
	run grepdata "FIELD\[Archiver | archivedby\]:"
	check_status
}

@test "Hold | customfield_12317326" {
	run grepdata "FIELD\[Hold | customfield_12317326\]:"
	check_status
}

@test "Watch List | customfield_12319745" {
	run grepdata "FIELD\[Watch List | customfield_12319745\]:"
	check_status
}

@test "RHEL Sub Components | customfield_12317327" {
	run grepdata "FIELD\[RHEL Sub Components | customfield_12317327\]:"
	check_status
}

@test "BZ Whiteboard | customfield_12317328" {
	run grepdata "FIELD\[BZ Whiteboard | customfield_12317328\]:"
	check_status
}

@test "Need Info Requestees | customfield_12317370" {
	run grepdata "FIELD\[Need Info Requestees | customfield_12317370\]:"
	check_status
}

@test "Product Affects Version | customfield_12317250" {
	run grepdata "FIELD\[Product Affects Version | customfield_12317250\]:"
	check_status
}

@test "Feature Link | customfield_12318341" {
	run grepdata "FIELD\[Feature Link | customfield_12318341\]:"
	check_status
}

@test "Git Commit | customfield_12317372" {
	run grepdata "FIELD\[Git Commit | customfield_12317372\]:"
	check_status
}

@test "Major Project | customfield_12320540" {
	run grepdata "FIELD\[Major Project | customfield_12320540\]:"
	check_status
}

@test "Target Milestone | customfield_12317251" {
	run grepdata "FIELD\[Target Milestone | customfield_12317251\]:"
	check_status
}

@test "Cross Team Epic | customfield_12318340" {
	run grepdata "FIELD\[Cross Team Epic | customfield_12318340\]:"
	check_status
}

@test "In Portfolio | customfield_12317252" {
	run grepdata "FIELD\[In Portfolio | customfield_12317252\]:"
	check_status
}

@test "MoSCoW | customfield_12316042" {
	run grepdata "FIELD\[MoSCoW | customfield_12316042\]:"
	check_status
}

@test "Ack'd Status | customfield_12317374" {
	run grepdata "FIELD\[Ack'd Status | customfield_12317374\]:"
	check_status
}

@test "Brief | customfield_12316043" {
	run grepdata "FIELD\[Brief | customfield_12316043\]:"
	check_status
}

@test "SP-Watchlist | customfield_12317253" {
	run grepdata "FIELD\[SP-Watchlist | customfield_12317253\]:"
	check_status
}

@test "Block/Fail - Additional Details | customfield_12317375" {
	run grepdata "FIELD\[Block/Fail - Additional Details | customfield_12317375\]:"
	check_status
}

@test "Watchlist Proposed Solution | customfield_12317254" {
	run grepdata "FIELD\[Watchlist Proposed Solution | customfield_12317254\]:"
	check_status
}

@test "Shepherd | customfield_12320544" {
	run grepdata "FIELD\[Shepherd | customfield_12320544\]:"
	check_status
}

@test "Automated Test | customfield_12320543" {
	run grepdata "FIELD\[Automated Test | customfield_12320543\]:"
	check_status
}

@test "Test Plan Created | customfield_12320542" {
	run grepdata "FIELD\[Test Plan Created | customfield_12320542\]:"
	check_status
}

@test "Design Doc Created | customfield_12320541" {
	run grepdata "FIELD\[Design Doc Created | customfield_12320541\]:"
	check_status
}

@test "Committed Version | customfield_12320548" {
	run grepdata "FIELD\[Committed Version | customfield_12320548\]:"
	check_status
}

@test "Product Lead | customfield_12320547" {
	run grepdata "FIELD\[Product Lead | customfield_12320547\]:"
	check_status
}

@test "Level of Effort | customfield_12320546" {
	run grepdata "FIELD\[Level of Effort | customfield_12320546\]:"
	check_status
}

@test "Interop Bug ID | customfield_12317376" {
	run grepdata "FIELD\[Interop Bug ID | customfield_12317376\]:"
	check_status
}

@test "Watchlist Impact | customfield_12317255" {
	run grepdata "FIELD\[Watchlist Impact | customfield_12317255\]:"
	check_status
}

@test "PM Business Priority | customfield_12317256" {
	run grepdata "FIELD\[PM Business Priority | customfield_12317256\]:"
	check_status
}

@test "Request Description | customfield_12317377" {
	run grepdata "FIELD\[Request Description | customfield_12317377\]:"
	check_status
}

@test "Customers | customfield_12317257" {
	run grepdata "FIELD\[Customers | customfield_12317257\]:"
	check_status
}

@test "Resolved Date | customfield_12317379" {
	run grepdata "FIELD\[Resolved Date | customfield_12317379\]:"
	check_status
}

@test "Pool Team | customfield_12317259" {
	run grepdata "FIELD\[Pool Team | customfield_12317259\]: rhel-sst-arch-hw"
	check_status
}

@test "Triage Status | customfield_12317380" {
	run grepdata "FIELD\[Triage Status | customfield_12317380\]:"
	check_status
}

@test "BZ Status | customfield_12317381" {
	run grepdata "FIELD\[BZ Status | customfield_12317381\]:"
	check_status
}

@test "Dev Approval | customfield_12317260" {
	run grepdata "FIELD\[Dev Approval | customfield_12317260\]:"
	check_status
}

@test "Docs Approval | customfield_12317261" {
	run grepdata "FIELD\[Docs Approval | customfield_12317261\]:"
	check_status
}

@test "Hierarchy Progress | customfield_12317140" {
	run grepdata "FIELD\[Hierarchy Progress | customfield_12317140\]:"
	check_status
}

@test "Hierarchy Progress Bar | customfield_12317141" {
	run grepdata "FIELD\[Hierarchy Progress Bar | customfield_12317141\]:"
	check_status
}

@test "PX Approval | customfield_12317262" {
	run grepdata "FIELD\[PX Approval | customfield_12317262\]:"
	check_status
}

@test "Test Failure Category | customfield_12320551" {
	run grepdata "FIELD\[Test Failure Category | customfield_12320551\]:"
	check_status
}

@test "PM Approval | customfield_12317263" {
	run grepdata "FIELD\[PM Approval | customfield_12317263\]:"
	check_status
}

@test "Planning Target | customfield_12319440" {
	run grepdata "FIELD\[Planning Target | customfield_12319440\]:"
	check_status
}

@test "RHV Progress | customfield_12317142" {
	run grepdata "FIELD\[RHV Progress | customfield_12317142\]:"
	check_status
}

@test "Requires_doc_text | customfield_12317384" {
	run grepdata "FIELD\[Requires_doc_text | customfield_12317384\]:"
	check_status
}

@test "QE Approval | customfield_12317264" {
	run grepdata "FIELD\[QE Approval | customfield_12317264\]:"
	check_status
}

@test "RHV Progress Bar | customfield_12317143" {
	run grepdata "FIELD\[RHV Progress Bar | customfield_12317143\]:"
	check_status
}

@test "Goal | customfield_12317386" {
	run grepdata "FIELD\[Goal | customfield_12317386\]:"
	check_status
}

@test "Start Date | customfield_12317265" {
	run grepdata "FIELD\[Start Date | customfield_12317265\]:"
	check_status
}

@test "Operating System | customfield_12320555" {
	run grepdata "FIELD\[Operating System | customfield_12320555\]:"
	check_status
}

@test "Orchestrator Version | customfield_12320554" {
	run grepdata "FIELD\[Orchestrator Version | customfield_12320554\]:"
	check_status
}

@test "Orchestrator | customfield_12320553" {
	run grepdata "FIELD\[Orchestrator | customfield_12320553\]:"
	check_status
}

@test "Cluster Flavor | customfield_12320552" {
	run grepdata "FIELD\[Cluster Flavor | customfield_12320552\]:"
	check_status
}

@test "Closed | customfield_12321641" {
	run grepdata "FIELD\[Closed | customfield_12321641\]:"
	check_status
}

@test "Release Delivery | customfield_12320558" {
	run grepdata "FIELD\[Release Delivery | customfield_12320558\]:"
	check_status
}

@test "Target Backport Versions | customfield_12323940" {
	run grepdata "FIELD\[Target Backport Versions | customfield_12323940\]:"
	check_status
}

@test "Docker Version | customfield_12320557" {
	run grepdata "FIELD\[Docker Version | customfield_12320557\]:"
	check_status
}

@test "Delivery Forecast Version | customfield_12320549" {
	run grepdata "FIELD\[Delivery Forecast Version | customfield_12320549\]:"
	check_status
}

@test "BZ QE Conditional NAK | customfield_12317267" {
	run grepdata "FIELD\[BZ QE Conditional NAK | customfield_12317267\]:"
	check_status
}

@test "Internal Target Milestone_old | customfield_12317146" {
	run grepdata "FIELD\[Internal Target Milestone_old | customfield_12317146\]:"
	check_status
}

@test "BZ Devel Whiteboard | customfield_12317268" {
	run grepdata "FIELD\[BZ Devel Whiteboard | customfield_12317268\]:"
	check_status
}

@test "Need Info From | customfield_12311840" {
	run grepdata "FIELD\[Need Info From | customfield_12311840\]:"
	check_status
}

@test "Organization Sponsor | customfield_12316140" {
	run grepdata "FIELD\[Organization Sponsor | customfield_12316140\]:"
	check_status
}

@test "Sponsor | customfield_12317350" {
	run grepdata "FIELD\[Sponsor | customfield_12317350\]:"
	check_status
}

@test "Product Sponsor | customfield_12316141" {
	run grepdata "FIELD\[Product Sponsor | customfield_12316141\]:"
	check_status
}

@test "Review Deadline | customfield_12317351" {
	run grepdata "FIELD\[Review Deadline | customfield_12317351\]:"
	check_status
}

@test "Severity | customfield_12316142" {
	run grepdata "FIELD\[Severity | customfield_12316142\]:"
	check_status
}

@test "Strategy Approved | customfield_12317352" {
	run grepdata "FIELD\[Strategy Approved | customfield_12317352\]:"
	check_status
}

@test "Corrective Measures - OLD | customfield_12322940" {
	run grepdata "FIELD\[Corrective Measures - OLD | customfield_12322940\]:"
	check_status
}

@test "DEV Story Points | customfield_12318444" {
	run grepdata "FIELD\[DEV Story Points | customfield_12318444\]:"
	check_status
}

@test "QE Story Points | customfield_12318443" {
	run grepdata "FIELD\[QE Story Points | customfield_12318443\]:"
	check_status
}

@test "Regression | customfield_12318446" {
	run grepdata "FIELD\[Regression | customfield_12318446\]:"
	check_status
}

@test "DOC Story Points | customfield_12318445" {
	run grepdata "FIELD\[DOC Story Points | customfield_12318445\]:"
	check_status
}

@test "Market Intelligence | customfield_12317357" {
	run grepdata "FIELD\[Market Intelligence | customfield_12317357\]:"
	check_status
}

@test "Function Impact | customfield_12317358" {
	run grepdata "FIELD\[Function Impact | customfield_12317358\]:"
	check_status
}

@test "Rank (Obsolete) | customfield_12310840" {
	run grepdata "FIELD\[Rank (Obsolete) | customfield_12310840\]: 9223372036854775807"
	check_status
}

@test "Test Blocker | customfield_12318448" {
	run grepdata "FIELD\[Test Blocker | customfield_12318448\]:"
	check_status
}

@test "Automated | customfield_12318447" {
	run grepdata "FIELD\[Automated | customfield_12318447\]:"
	check_status
}

@test "oVirt Team | customfield_12317359" {
	run grepdata "FIELD\[oVirt Team | customfield_12317359\]:"
	check_status
}

@test "CDW support_ack | customfield_12318449" {
	run grepdata "FIELD\[CDW support_ack | customfield_12318449\]:"
	check_status
}

@test "Doc Contact | customfield_12317360" {
	run grepdata "FIELD\[Doc Contact | customfield_12317360\]:"
	check_status
}

@test "Deployment Environment | customfield_12319540" {
	run grepdata "FIELD\[Deployment Environment | customfield_12319540\]:"
	check_status
}

@test "Involved teams | customfield_12317361" {
	run grepdata "FIELD\[Involved teams | customfield_12317361\]:"
	check_status
}

@test "Additional watchers | customfield_12317362" {
	run grepdata "FIELD\[Additional watchers | customfield_12317362\]:"
	check_status
}

@test "Fixed in Build | customfield_12318450" {
	run grepdata "FIELD\[Fixed in Build | customfield_12318450\]:"
	check_status
}

@test "External issue URL | customfield_12317242" {
	run grepdata "FIELD\[External issue URL | customfield_12317242\]:"
	check_status
}

@test "Fixed In Version | customfield_12317363" {
	run grepdata "FIELD\[Fixed In Version | customfield_12317363\]:"
	check_status
}

@test "ServiceNow ID | customfield_12317243" {
	run grepdata "FIELD\[ServiceNow ID | customfield_12317243\]:"
	check_status
}

@test "Testable Builds | customfield_12321740" {
	run grepdata "FIELD\[Testable Builds | customfield_12321740\]:"
	check_status
}

@test "SourceForge Reference | customfield_10002" {
	run grepdata "FIELD\[SourceForge Reference | customfield_10002\]:"
	check_status
}

@test "Gerrit Link | customfield_12317244" {
	run grepdata "FIELD\[Gerrit Link | customfield_12317244\]:"
	check_status
}

@test "ACKs Check | customfield_12317366" {
	run grepdata "FIELD\[ACKs Check | customfield_12317366\]:"
	check_status
}

@test "Mojo Link | customfield_12317245" {
	run grepdata "FIELD\[Mojo Link | customfield_12317245\]:"
	check_status
}

@test "Needs Info | customfield_12317246" {
	run grepdata "FIELD\[Needs Info | customfield_12317246\]:"
	check_status
}

@test "[QE] How to address? | customfield_12317367" {
	run grepdata "FIELD\[\[QE\] How to address? | customfield_12317367\]:"
	check_status
}

@test "Discussion Needed | customfield_12317247" {
	run grepdata "FIELD\[Discussion Needed | customfield_12317247\]:"
	check_status
}

@test "[QE] Why QE missed? | customfield_12317368" {
	run grepdata "FIELD\[\[QE\] Why QE missed? | customfield_12317368\]:"
	check_status
}

@test "CDW docs_ack | customfield_12311941" {
	run grepdata "FIELD\[CDW docs_ack | customfield_12311941\]:"
	check_status
}

@test "Discussion Occurred | customfield_12317248" {
	run grepdata "FIELD\[Discussion Occurred | customfield_12317248\]:"
	check_status
}

@test "RCA Description | customfield_12317369" {
	run grepdata "FIELD\[RCA Description | customfield_12317369\]:"
	check_status
}

@test "Contributing Teams | customfield_12317249" {
	run grepdata "FIELD\[Contributing Teams | customfield_12317249\]:"
	check_status
}

@test "Rank | customfield_12311940" {
	run grepdata "FIELD\[Rank | customfield_12311940\]: 1|hy9spr:"
	check_status
}

@test "Action | customfield_12317291" {
	run grepdata "FIELD\[Action | customfield_12317291\]:"
	check_status
}

@test "Dev Target Milestone | customfield_12318141" {
	run grepdata "FIELD\[Dev Target Milestone | customfield_12318141\]:"
	check_status
}

@test "Reset contact to default | customfield_12322640" {
	run grepdata "FIELD\[Reset contact to default | customfield_12322640\]:"
	check_status
}

@test "prev_assignee | customfield_12318140" {
	run grepdata "FIELD\[prev_assignee | customfield_12318140\]:"
	check_status
}

@test "Biz Program Manager | customfield_12318143" {
	run grepdata "FIELD\[Biz Program Manager | customfield_12318143\]:"
	check_status
}

@test "PLM  Technical Lead | customfield_12318142" {
	run grepdata "FIELD\[PLM  Technical Lead | customfield_12318142\]:"
	check_status
}

@test "Planned Start | customfield_12317296" {
	run grepdata "FIELD\[Planned Start | customfield_12317296\]:"
	check_status
}

@test "Product Page link | customfield_12318145" {
	run grepdata "FIELD\[Product Page link | customfield_12318145\]:"
	check_status
}

@test "Solution | customfield_12317298" {
	run grepdata "FIELD\[Solution | customfield_12317298\]:"
	check_status
}

@test "Tech Program Manager | customfield_12318144" {
	run grepdata "FIELD\[Tech Program Manager | customfield_12318144\]:"
	check_status
}

@test "Reminder Date | customfield_12317290" {
	run grepdata "FIELD\[Reminder Date | customfield_12317290\]:"
	check_status
}

@test "Test Work Items | customfield_12312840" {
	run grepdata "FIELD\[Test Work Items | customfield_12312840\]:"
	check_status
}

@test "Internal Product/Project names | customfield_12318147" {
	run grepdata "FIELD\[Internal Product/Project names | customfield_12318147\]:"
	check_status
}

@test "Latest Status Summary | customfield_12317299" {
	run grepdata "FIELD\[Latest Status Summary | customfield_12317299\]:"
	check_status
}

@test "Portal Product Name | customfield_12318146" {
	run grepdata "FIELD\[Portal Product Name | customfield_12318146\]:"
	check_status
}

@test "QE Test Coverage | customfield_12312848" {
	run grepdata "FIELD\[QE Test Coverage | customfield_12312848\]:"
	check_status
}

@test "Stale Date | customfield_12318148" {
	run grepdata "FIELD\[Stale Date | customfield_12318148\]:"
	check_status
}

@test "Approver | customfield_12318150" {
	run grepdata "FIELD\[Approver | customfield_12318150\]:"
	check_status
}

@test "Internal Target Milestone numeric | customfield_12321440" {
	run grepdata "FIELD\[Internal Target Milestone numeric | customfield_12321440\]:"
	check_status
}

@test "DevOps Discussion Occurred | customfield_12319241" {
	run grepdata "FIELD\[DevOps Discussion Occurred | customfield_12319241\]:"
	check_status
}

@test "EAP PT Cross Product Agreement (CPA) | customfield_12321441" {
	run grepdata "FIELD\[EAP PT Cross Product Agreement (CPA) | customfield_12321441\]:"
	check_status
}

@test "MVP Status | customfield_12318152" {
	run grepdata "FIELD\[MVP Status | customfield_12318152\]:"
	check_status
}

@test "Organization ID | customfield_12323741" {
	run grepdata "FIELD\[Organization ID | customfield_12323741\]:"
	check_status
}

@test "Software | customfield_12323740" {
	run grepdata "FIELD\[Software | customfield_12323740\]:"
	check_status
}

@test "Design Review Sign-off | customfield_12319242" {
	run grepdata "FIELD\[Design Review Sign-off | customfield_12319242\]:"
	check_status
}

@test "Risk Impact | customfield_12318156" {
	run grepdata "FIELD\[Risk Impact | customfield_12318156\]:"
	check_status
}

@test "Risk Likelihood | customfield_12318155" {
	run grepdata "FIELD\[Risk Likelihood | customfield_12318155\]:"
	check_status
}

@test "Impacts | customfield_12313940" {
	run grepdata "FIELD\[Impacts | customfield_12313940\]:"
	check_status
}

@test "Security Sensitive Issue | customfield_12311640" {
	run grepdata "FIELD\[Security Sensitive Issue | customfield_12311640\]:"
	check_status
}

@test "Involved | customfield_12311641" {
	run grepdata "FIELD\[Involved | customfield_12311641\]:"
	check_status
}

@test "Next Planned Release Date | customfield_12319247" {
	run grepdata "FIELD\[Next Planned Release Date | customfield_12319247\]:"
	check_status
}

@test "Risk Mitigation Strategy | customfield_12318158" {
	run grepdata "FIELD\[Risk Mitigation Strategy | customfield_12318158\]:"
	check_status
}

@test "Risk Score Assessment | customfield_12318157" {
	run grepdata "FIELD\[Risk Score Assessment | customfield_12318157\]:"
	check_status
}

@test "Avoidable | customfield_12319249" {
	run grepdata "FIELD\[Avoidable | customfield_12319249\]:"
	check_status
}

@test "Target end | customfield_12313942" {
	run grepdata "FIELD\[Target end | customfield_12313942\]:"
	check_status
}

@test "Target start | customfield_12313941" {
	run grepdata "FIELD\[Target start | customfield_12313941\]:"
	check_status
}

@test "BZ Devel Conditional NAK | customfield_12317270" {
	run grepdata "FIELD\[BZ Devel Conditional NAK | customfield_12317270\]:"
	check_status
}

@test "CDW Resolution | customfield_12317392" {
	run grepdata "FIELD\[CDW Resolution | customfield_12317392\]:"
	check_status
}

@test "mvp | customfield_12317271" {
	run grepdata "FIELD\[mvp | customfield_12317271\]:"
	check_status
}

@test "rpl | customfield_12317272" {
	run grepdata "FIELD\[rpl | customfield_12317272\]:"
	check_status
}

@test "Verified | customfield_12317273" {
	run grepdata "FIELD\[Verified | customfield_12317273\]:"
	check_status
}

@test "Documenter | customfield_12318241" {
	run grepdata "FIELD\[Documenter | customfield_12318241\]:"
	check_status
}

@test "ITR-ITM | customfield_12317396" {
	run grepdata "FIELD\[ITR-ITM | customfield_12317396\]:"
	check_status
}

@test "Analyst | customfield_12318243" {
	run grepdata "FIELD\[Analyst | customfield_12318243\]:"
	check_status
}

@test "Internal Target Release and Milestone | customfield_12317277" {
	run grepdata "FIELD\[Internal Target Release and Milestone | customfield_12317277\]:"
	check_status
}

@test "Current Deadline | customfield_12317278" {
	run grepdata "FIELD\[Current Deadline | customfield_12317278\]:"
	check_status
}

@test "Global Practices Lead | customfield_12318245" {
	run grepdata "FIELD\[Global Practices Lead | customfield_12318245\]:"
	check_status
}

@test "Current Deadline Type | customfield_12317279" {
	run grepdata "FIELD\[Current Deadline Type | customfield_12317279\]:"
	check_status
}

@test "Eng. priority | customfield_12312940" {
	run grepdata "FIELD\[Eng. priority | customfield_12312940\]:"
	check_status
}

@test "QE priority | customfield_12312941" {
	run grepdata "FIELD\[QE priority | customfield_12312941\]:"
	check_status
}

@test "Embargo Override | customfield_12317281" {
	run grepdata "FIELD\[Embargo Override | customfield_12317281\]:"
	check_status
}

@test "Baseline Start | customfield_12317282" {
	run grepdata "FIELD\[Baseline Start | customfield_12317282\]:"
	check_status
}

@test "Preliminary Testing | customfield_12321540" {
	run grepdata "FIELD\[Preliminary Testing | customfield_12321540\]:"
	check_status
}

@test "UXD Design[Test] | customfield_12319340" {
	run grepdata "FIELD\[UXD Design\[Test\] | customfield_12319340\]:"
	check_status
}

@test "Watchers Groups | customfield_12323840" {
	run grepdata "FIELD\[Watchers Groups | customfield_12323840\]:"
	check_status
}

@test "Baseline End | customfield_12317283" {
	run grepdata "FIELD\[Baseline End | customfield_12317283\]:"
	check_status
}

@test "EAP PT Feature Implementation (FI) | customfield_12317041" {
	run grepdata "FIELD\[EAP PT Feature Implementation (FI) | customfield_12317041\]:"
	check_status
}

@test "Errata Link | customfield_12321541" {
	run grepdata "FIELD\[Errata Link | customfield_12321541\]:"
	check_status
}

@test "Docs Analysis | customfield_12317042" {
	run grepdata "FIELD\[Docs Analysis | customfield_12317042\]:"
	check_status
}

@test "Subproject | customfield_12317284" {
	run grepdata "FIELD\[Subproject | customfield_12317284\]:"
	check_status
}

@test "Root Cause | customfield_12317285" {
	run grepdata "FIELD\[Root Cause | customfield_12317285\]:"
	check_status
}

@test "Stage Links | customfield_12317043" {
	run grepdata "FIELD\[Stage Links | customfield_12317043\]:"
	check_status
}

@test "Docs Pull Request | customfield_12317044" {
	run grepdata "FIELD\[Docs Pull Request | customfield_12317044\]:"
	check_status
}

@test "Project/s | customfield_12317286" {
	run grepdata "FIELD\[Project/s | customfield_12317286\]:"
	check_status
}

@test "Authorized Party | customfield_12321542" {
	run grepdata "FIELD\[Authorized Party | customfield_12321542\]:"
	check_status
}

@test "User Story | customfield_12311740" {
	run grepdata "FIELD\[User Story | customfield_12311740\]:"
	check_status
}

@test "Reminder Frequency | customfield_12317288" {
	run grepdata "FIELD\[Reminder Frequency | customfield_12317288\]:"
	check_status
}

@test "QE Acceptance Test Link | customfield_12311741" {
	run grepdata "FIELD\[QE Acceptance Test Link | customfield_12311741\]:"
	check_status
}

@test "Partner Fixed Version | customfield_12322440" {
	run grepdata "FIELD\[Partner Fixed Version | customfield_12322440\]:"
	check_status
}

@test "Scoping Status | customfield_12319272" {
	run grepdata "FIELD\[Scoping Status | customfield_12319272\]:"
	check_status
}

@test "QE_AUTO_Coverage | customfield_12319271" {
	run grepdata "FIELD\[QE_AUTO_Coverage | customfield_12319271\]:"
	check_status
}

@test "Function | customfield_12319274" {
	run grepdata "FIELD\[Function | customfield_12319274\]:"
	check_status
}

@test "Stakeholders | customfield_12319273" {
	run grepdata "FIELD\[Stakeholders | customfield_12319273\]:"
	check_status
}

@test "Workstream | customfield_12319275" {
	run grepdata "FIELD\[Workstream | customfield_12319275\]:"
	check_status
}

@test "Version | customfield_12319278" {
	run grepdata "FIELD\[Version | customfield_12319278\]:"
	check_status
}

@test "CVSS Score | customfield_12324748" {
	run grepdata "FIELD\[CVSS Score | customfield_12324748\]:"
	check_status
}

@test "CWE ID | customfield_12324747" {
	run grepdata "FIELD\[CWE ID | customfield_12324747\]:"
	check_status
}

@test "CVE ID | customfield_12324749" {
	run grepdata "FIELD\[CVE ID | customfield_12324749\]:"
	check_status
}

@test "Security Documentation Type | customfield_12319270" {
	run grepdata "FIELD\[Security Documentation Type | customfield_12319270\]:"
	check_status
}

@test "Source | customfield_12324746" {
	run grepdata "FIELD\[Source | customfield_12324746\]:"
	check_status
}

@test "Git Pull Request | customfield_12310220" {
	run grepdata "FIELD\[Git Pull Request | customfield_12310220\]:"
	check_status
}

@test "ID | customfield_12319279" {
	run grepdata "FIELD\[ID | customfield_12319279\]:"
	check_status
}

@test "Upstream Affected Component | customfield_12324751" {
	run grepdata "FIELD\[Upstream Affected Component | customfield_12324751\]:"
	check_status
}

@test "Embargo Status | customfield_12324750" {
	run grepdata "FIELD\[Embargo Status | customfield_12324750\]:"
	check_status
}

@test "Products | customfield_12319040" {
	run grepdata "FIELD\[Products | customfield_12319040\]: Red Hat Enterprise Linux"
	check_status
}

@test "Images | thumbnail" {
	run grepdata "FIELD\[Images | thumbnail\]:"
	check_status
}

@test "PX Technical Impact Notes-Obsolete | customfield_12319285" {
	run grepdata "FIELD\[PX Technical Impact Notes-Obsolete | customfield_12319285\]:"
	check_status
}

@test "Special Handling | customfield_12324753" {
	run grepdata "FIELD\[Special Handling | customfield_12324753\]:"
	check_status
}

@test "Downstream Component Name | customfield_12324752" {
	run grepdata "FIELD\[Downstream Component Name | customfield_12324752\]:"
	check_status
}

@test "Test Link | customfield_12325840" {
	run grepdata "FIELD\[Test Link | customfield_12325840\]:"
	check_status
}

@test "Docs Impact Notes | customfield_12319287" {
	run grepdata "FIELD\[Docs Impact Notes | customfield_12319287\]:"
	check_status
}

@test "User Domain | customfield_12319045" {
	run grepdata "FIELD\[User Domain | customfield_12319045\]:"
	check_status
}

@test "Docs Impact | customfield_12319286" {
	run grepdata "FIELD\[Docs Impact | customfield_12319286\]: Unspecified"
	check_status
}

@test "User | customfield_12319044" {
	run grepdata "FIELD\[User | customfield_12319044\]:"
	check_status
}

@test "Marketing Impact Notes | customfield_12319289" {
	run grepdata "FIELD\[Marketing Impact Notes | customfield_12319289\]:"
	check_status
}

@test "GtmhubTaskParentType | customfield_12321240" {
	run grepdata "FIELD\[GtmhubTaskParentType | customfield_12321240\]:"
	check_status
}

@test "Marketing Impact | customfield_12319288" {
	run grepdata "FIELD\[Marketing Impact | customfield_12319288\]:"
	check_status
}

@test "Impact Rating | customfield_12324755" {
	run grepdata "FIELD\[Impact Rating | customfield_12324755\]:"
	check_status
}

@test "Errata ID | customfield_12324754" {
	run grepdata "FIELD\[Errata ID | customfield_12324754\]:"
	check_status
}

@test "Approvals | customfield_12310110" {
	run grepdata "FIELD\[Approvals | customfield_12310110\]:"
	check_status
}

@test "Docs QE Status | customfield_12310230" {
	run grepdata "FIELD\[Docs QE Status | customfield_12310230\]:"
	check_status
}

@test "Migration Text | customfield_12313740" {
	run grepdata "FIELD\[Migration Text | customfield_12313740\]:"
	check_status
}

@test "Communication Breakdown | customfield_12319250" {
	run grepdata "FIELD\[Communication Breakdown | customfield_12319250\]:"
	check_status
}

@test "affectsRHMIVersion | customfield_12318040" {
	run grepdata "FIELD\[affectsRHMIVersion | customfield_12318040\]:"
	check_status
}

@test "Product Operations Engineering Contact | customfield_12322540" {
	run grepdata "FIELD\[Product Operations Engineering Contact | customfield_12322540\]:"
	check_status
}

@test "Release Milestone | customfield_12319252" {
	run grepdata "FIELD\[Release Milestone | customfield_12319252\]:"
	check_status
}

@test "QE Properly Involved | customfield_12319251" {
	run grepdata "FIELD\[QE Properly Involved | customfield_12319251\]:"
	check_status
}

@test "Strategic Alignment | customfield_12324840" {
	run grepdata "FIELD\[Strategic Alignment | customfield_12324840\]:"
	check_status
}

@test "affectsRHOAMVersion | customfield_12318041" {
	run grepdata "FIELD\[affectsRHOAMVersion | customfield_12318041\]:"
	check_status
}

@test "Bugzilla References | customfield_12310440" {
	run grepdata "FIELD\[Bugzilla References | customfield_12310440\]:"
	check_status
}

@test "Security Level | security" {
	run grepdata "FIELD\[Security Level | security\]: Restricts access to Red Hat employees who are approved to view product development information"
	check_status
}

@test "Attachment | attachment" {
	run grepdata "FIELD\[Attachment | attachment\]:"
	check_status
}

@test "Delivery Mode | customfield_12323640" {
	run grepdata "FIELD\[Delivery Mode | customfield_12323640\]:"
	check_status
}

@test "VEX Justification | customfield_12325940" {
	run grepdata "FIELD\[VEX Justification | customfield_12325940\]:"
	check_status
}

@test "Chapter # | customfield_12323642" {
	run grepdata "FIELD\[Chapter # | customfield_12323642\]:"
	check_status
}

@test "Proposed Initiative | customfield_12319263" {
	run grepdata "FIELD\[Proposed Initiative | customfield_12319263\]:"
	check_status
}

@test "Keyword | customfield_12319262" {
	run grepdata "FIELD\[Keyword | customfield_12319262\]:"
	check_status
}

@test "Workaround | customfield_12323641" {
	run grepdata "FIELD\[Workaround | customfield_12323641\]:"
	check_status
}

@test "Immutable Due Date | customfield_12319264" {
	run grepdata "FIELD\[Immutable Due Date | customfield_12319264\]:"
	check_status
}

@test "Failure Category | customfield_12319267" {
	run grepdata "FIELD\[Failure Category | customfield_12319267\]:"
	check_status
}

@test "Epic Color | customfield_12323648" {
	run grepdata "FIELD\[Epic Color | customfield_12323648\]:"
	check_status
}

@test "Language | customfield_12323647" {
	run grepdata "FIELD\[Language | customfield_12323647\]:"
	check_status
}

@test "RH Private Keywords | customfield_12323649" {
	run grepdata "FIELD\[RH Private Keywords | customfield_12323649\]:"
	check_status
}

@test "Section ID | customfield_12323644" {
	run grepdata "FIELD\[Section ID | customfield_12323644\]:"
	check_status
}

@test "Section Title | customfield_12323643" {
	run grepdata "FIELD\[Section Title | customfield_12323643\]:"
	check_status
}

@test "URL | customfield_12323646" {
	run grepdata "FIELD\[URL | customfield_12323646\]:"
	check_status
}

@test "Reporter RHN ID | customfield_12323645" {
	run grepdata "FIELD\[Reporter RHN ID | customfield_12323645\]:"
	check_status
}

@test "Notes | customfield_12313841" {
	run grepdata "FIELD\[Notes | customfield_12313841\]:"
	check_status
}

@test "sprint_count | customfield_12319269" {
	run grepdata "FIELD\[sprint_count | customfield_12319269\]:"
	check_status
}

@test "QE portion | customfield_12319268" {
	run grepdata "FIELD\[QE portion | customfield_12319268\]:"
	check_status
}

@test "Release Note Status | customfield_12310213" {
	run grepdata "FIELD\[Release Note Status | customfield_12310213\]:"
	check_status
}

@test "Writer | customfield_12310214" {
	run grepdata "FIELD\[Writer | customfield_12310214\]:"
	check_status
}

